<?php

// Environment variable
//  Etc, define("DEBUG", true)
if (file_exists(".." . DIRECTORY_SEPARATOR . ".env.php")) {
    require_once ".." . DIRECTORY_SEPARATOR . ".env.php";
}
if (defined("DEBUG") and DEBUG) {
    ini_set('display_errors', 1);
    ini_set('html_errors', true);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

require __DIR__.DIRECTORY_SEPARATOR.'vendor'. DIRECTORY_SEPARATOR.'autoload.php';

use Whoops\Run as WhoopsRun;
use Whoops\Handler\PrettyPageHandler as WhoopsPrettyPageHandler;
use Hybridauth\Hybridauth;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Database\Capsule\Manager as Capsule;

$whoops = new WhoopsRun;
$whoops->pushHandler(new WhoopsPrettyPageHandler);
$whoops->register();

try {
    $capsule = new Capsule;
    $capsule->addConnection([
        'driver'    => DB_DRIVER,
        'host'      => DB_HOST,
        'database'  => DB_PATH,
        'username'  => DB_USERNAME,
        'password'  => DB_PASSWORD,
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ]);

    // // Register a new user
    // Sentinel::register([
    //     'email'    => 'test@example.com',
    //     'password' => 'foobar',
    // ]);

    // Make this Capsule instance available globally via static methods
    $capsule->setAsGlobal();
    // Setup the Eloquent ORM
    $capsule->bootEloquent();
}
catch(Exception $e) {
    $whoops->handleException($e);
}

function dd($var) {
    var_dump($var);
    exit();
}