<style>
    .site-hero {
        background-size: none !important;
        background-position: none;
        min-height: 0;
        height: auto;
    }
</style>
<section class="site-hero" style="background-position: 50% -25px;">
    <header role="banner">
        <!-- background: linear-gradient(440deg, rgba(255,255,255,0.224) 0% -->
        <style>
            .site-hero header[role=banner] nav li a {
                color: rgba(0, 0, 0, 0.8) !important;
            }
        </style>
        <nav class="navbar navbar-expand-lg" style="background: none !important;box-shadow: 0 0 5px #888;background-image: url(https://www.transparenttextures.com/patterns/ag-square.png) !important;">
            <img class="header-logo" src="images/logo/color.png" alt="Image placeholder">
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbars-05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse navbar-light collapse" id="navbars-05">
                <ul class="navbar-nav">
                    <li class="nav-item <?= $nav_active_home_page ?>">
                        <a class="nav-link" href="<?= $url_home_page ?>">Home</a>
                    </li>
                    <li class="nav-item <?= $nav_active_about_us_page ?>">
                        <a class="nav-link" href="<?= $url_about_us_page ?>">About Us</a>
                    </li>
                    <li class="nav-item dropdown <?= $nav_active_services_page ?>">
                        <a id="dropdown-services" aria-haspopup="true" aria-expanded="false" class=" nav-link dropdown-toggle <?= $nav_active_home_page ?>" data-toggle="dropdown" href="<?= $url_home_page ?>">Services</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-services">
                            <h2 class="dropdown-header">Teledental</h2>
                            <a class="dropdown-item" href="<?= $url_services_page ?>">General Consultation</a>
                            <a class="dropdown-item" href="#services#second-opinion">Get A Second Opinion</a>
                            <a class="dropdown-item" href="#services#tele-aligner">Tele-Aligner</a>
                            <div class="dropdown-divider"></div>
                            <h2 class="dropdown-header">Others</h2>
                            <a class="dropdown-item" href="#services#promotion">Promotion</a>
                            <a class="dropdown-item" href="#services#events">Event</a>
                        </div>
                    </li>
                    <li class="nav-item <?= $nav_active_treatment_page ?>">
                        <a class="nav-link" href="<?= $url_treatment_page ?>">Treatment</a>
                    </li>
                    <li class="nav-item <?= $nav_active_contact_us_page ?>">
                        <a class="nav-link" href="<?= $url_contact_us_page ?>">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=" <?= $url_login_page ?>">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=" <?= $url_register_page ?>">Register</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- END page_top_navigation -->
</section>