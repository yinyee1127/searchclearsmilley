<style>
    /* The switch - the box around the slider */
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    /* Hide default HTML checkbox */
    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    /* The slider */
    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<div class="site-section animated-element">
    <div class="container">
        <div class="block-17">
            <!-- <h2>Search by teledentist</h2> -->
            <form action="<?= $url_post_search_result ?>" method="post" class="d-block d-lg-flex" style="border-radius: 8px">
                <div class="fields d-block d-lg-flex">
                    <div class="textfield-search one-third">
                        <input type="text" name="location" class="form-control" placeholder="Location" value="<?= $variable_location ?>">
                    </div>
                    <div class="textfield-search one-third">
                        <input type="text" name="clinic_name" class="form-control" placeholder="Clinic name" value="<?= $variable_clinic_name ?>">
                    </div>
                    <div class="textfield-search one-third">
                        <input type="text" name="treatment_option" class="form-control" placeholder="Treatment" value="<?= $variable_treatment_option ?>">
                    </div>
                    <!-- width percentage100 -->
                </div>
                <input type="submit" class="search-submit btn btn-primary" value="Search" style="width:auto;">
            </form>
            <!-- Rounded switch -->
            <label class="switch">
                <input type="checkbox">
                <span class="slider round"></span>
            </label>
        </div>
    </div>
</div>