<?php
// include('../template/pages/controller.php');

use Hybridauth\Hybridauth;

global $CONFIG_SOCIAL;
$hybridauth = new Hybridauth($CONFIG_SOCIAL);
$adapters = $hybridauth->getConnectedAdapters();
?>
<section class="site-section" style="background-image: url(images/background/bg5.jpg);background-size:cover">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-7">
        <div class="form-wrap login">
          <h2 class="h2 register" style="margin-bottom:16px">Login Now!</h2>
          <h4 class="h4 register">Log in to your account to access more features in Findentist.</h4><br>
          <ul>
              <?php
              if ($adapters) {
                echo
                '
                Your account have been registered. Please click the Login button below to login.
                <span>(<a href="/logout">Log Out</a>)</span>
                ';
                // foreach ($adapters as $name => $adapter) {
                //   echo '
                //   <li>
                //     Welcome, ' . $adapter->getUserProfile()->displayName . ' from <b>' . $name . '</b>
                //     <img src="' . $adapter->getUserProfile()->photoURL . '" style="height: 50px">
                //     <span>(<a href="/logout">Log Out</a>)</span>
                //   </li>
                //   ';
                // }
              }
              ?>
            </ul>

          <form action="/login" method="POST">
            <div class="row">
              <div class="col-md-12 form-group">
                <label for="name">Email</label>
                <input type="text" id="name" name="email" class="form-control register py-2">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <label for="name">Password</label>
                <input type="password" id="name" name="password" class="form-control register py-2">
              </div>
            </div>
            <div class="row mb-1">
              <div class="col-md-12 form-group">
                <input type="checkbox" id="remember" name="remember" value="signin">
                <label for="remember"> Stay signed in.</label><br>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <input type="submit" name="login" value="Login" class="btn btn-primary px-5 py-2 button login">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <div class="text-center">Or </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <a href="<?= $CONFIG_SOCIAL["callback"]; ?>?provider=Google" class="btn btn-primary px-5 py-2 button login" role="button"><i class="fa fa-google padding10 text-center"></i> Continue with Google</a>
              </div>
            </div>
            <div class="row mb-1">
              <div class="col-md-12 form-group">
                <a href="<?= $CONFIG_SOCIAL["callback"]; ?>?provider=Facebook" class="btn btn-primary px-5 py-2 button login" role="button"><i class="fa fa-facebook padding10 text-center"></i> Continue with Facebook</a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <p><a href="/forgot-password">Forgot Password?</a> </p>
                <p>Not yet a member? Click <a href="/">here</a> to register. </p>
                <p><a href='/forgot-password'>Resend Comformation Email.</a></p>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>