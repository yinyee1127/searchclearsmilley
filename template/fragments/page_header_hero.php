<!-- END header -->
<section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(images/background/bg1-1200x798.jpg);">
    <header role="banner">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <img class="header-logo" src="images/logo/white.png" alt="Image placeholder" class="img-fluid">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbars-05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse navbar-light" id="navbars-05">
                <ul class="navbar-nav">
                    <li class="nav-item <?= $nav_active_home_page ?>">
                        <a class="nav-link" href="<?= $url_home_page ?>">Home</a>
                    </li>
                    <li class="nav-item <?= $nav_active_about_us_page ?>">
                        <a class="nav-link" href="<?= $url_about_us_page ?>">About Us</a>
                    </li>
                    <li class="nav-item dropdown <?= $nav_active_services_page ?>">
                        <a id="dropdown-services" aria-haspopup="true" aria-expanded="false" class=" nav-link dropdown-toggle <?= $nav_active_home_page ?>" data-toggle="dropdown" href="<?= $url_home_page ?>">Services</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-services">
                            <h2 class="dropdown-header">Teledental</h2>
                            <a class="dropdown-item" href="#services#general-consultation">General Consultation</a>
                            <a class="dropdown-item" href="#services#second-opinion">Get A Second Opinion</a>
                            <a class="dropdown-item" href="#services#tele-aligner">Tele-Aligner</a>
                            <div class="dropdown-divider"></div>
                            <h2 class="dropdown-header">Others</h2>
                            <a class="dropdown-item" href="#services#promotion">Promotion</a>
                            <a class="dropdown-item" href="#services#events">Event</a>
                        </div>
                    </li>
                    <li class="nav-item <?= $nav_active_treatment_page ?>">
                        <a class="nav-link" href="<?= $url_treatment_page ?>">Treatment</a>
                    </li>
                    <li class="nav-item <?= $nav_active_contact_us_page ?>">
                        <a class="nav-link" href="<?= $url_contact_us_page ?>">Contact Us</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=" <?= $url_login_page ?>">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href=" <?= $url_register_page ?>">Register</a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- END page_top_navigation -->
    <div class="container">
        <style>
            @media screen and (max-width: 540px) {

                /* Increase specificity */
                div.container .form-wrap {
                    width: 100% !important;
                    height: auto !important;
                }
            }

            @media (min-width: 768px) {
                .md-pr-0 {
                    padding-right: 0;
                    border-right-width: 1px !important;
                }

                .md-pl-0 {
                    padding-left: 0;
                }

                /* .form-wrap form .form-control {
                    border-right-width: 1px !important;
                    border-right-style: solid !important;
                    border-right-color: rgba(0, 0, 0, 0.8) !important;
                } */
            }

            .form-wrap h2.title {
                font-size: 32px;
                line-height: 36px !important;
                font-weight: 700;
                color: rgba(0, 0, 0, 0.8);
                margin: 0;
                margin-bottom: 8px !important;
                padding: 0;
            }

            .form-wrap .description {
                font-size: 18px;
                font-weight: 400;
                color: rgba(0, 0, 0, 0.8);
                line-height: 26px !important;
                margin-bottom: 8px !important;
                padding: 0 !important;
            }

            .form-wrap form .custom-select,
            .form-wrap form .custom-select option {
                margin-bottom: 4px;
                font-size: 16px;
                font-weight: 600;
                background: none;
                color: rgba(0, 0, 0, 0.8) !important;
            }

            .form-wrap .pr-0 {
                padding-right: 0;
            }

            .form-wrap .pl-0 {
                padding-left: 0;
            }

            .form-wrap.mt-30 {
                margin-top: 48px !important;
            }

            .form-wrap .mb-8 {
                margin-bottom: 8px;
            }

            .form-wrap .bdr-0 {
                border-right: 0 !important;
            }

            .form-wrap form .label {
                font-size: 12px;
                font-weight: 700;
                color: rgba(0, 0, 0, 0.8);
                margin: 0;
                margin-bottom: 8px;
            }

            .form-wrap form btn.search {
                font-size: 16px;
                font-weight: 600;
                margin: 0;
            }

            .form-wrap form .form-control {
                font-size: 16px;
                font-weight: 600;
                color: rgba(0, 0, 0, 0.8);
                margin: 0;
                margin-bottom: 16px;

                
            }
            .width100{
                width:100%
            }
            
        </style>
        <div class="form-wrap mt-30">
            <h2 class="title">Find A Dentist in Malaysia on Findentist</h2>
            <div class="description">
                Search for any doctor that meets your trust and convenience without breaking the bank.
            </div>
            <form action="<?= $url_post_search_result ?>" method="post">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="form-check">
                            <input class="custom-radiobox form-check-input" type="radio" name="search" value="normal" checked="" style="margin-top: 10px;">
                            <label class="form-check-label" for="normal" style="font-weight: 600;">
                                I'm looking for a clinic/doctor
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="custom-radiobox form-check-input" type="radio" name="search" value="teledental" style="margin-top: 10px;">
                            <label class="form-check-label" for="teledental" style="font-weight: 600;">
                                I'm looking for teledental services
                            </label>
                        </div>
                        <a href="index.html">What is this?</a>
                    </div>
                </div>
                <div class="row option-normal">
                    <div class="col-sm-12 col-md-6 md-pr-0">
                        <div class="label">CLINIC NAME</div>
                        <input class="form-control bdr-0" type="text" name="clinic_name" autocomplete="off">
                    </div>
                    <div class="col-sm-12 col-md-6 md-pl-0">
                        <div class="label">TREATMENT</div>
                        <input class="form-control" type="text" name="treatment_option" autocomplete="off">
                    </div>
                </div>
                <div class="row option-normal">
                    <div class="col-md-12">
                        <div class="label">LOCATION</div>
                        <input class="form-control" type="text" name="location" autocomplete="off">
                    </div>
                </div>
                <div class="row option-teledental" style="display:none">
                    <div class="col-sm-12 col-md-12">
                        <div class="label">SERVICE TYPE</div>
                        <select class="custom-select">
                            <option value="general">I want general consultation on a minor issue</option>
                            <option value="teledental">I want a second opinon on a medical issue</option>
                            <option value="teledental">I want to align teeth without clinic visit</option>
                        </select>
                        <a href="index.html">More Info</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" value="Search" class="btn btn-primary search width100">
                    </div>
                </div>
                <div class="row">
                    <a href="index.html" style="text-align: center;padding-top: 8px;font-weight: 600;">
                        Have an idea to improve findentist? We want your feedback!
                    </a>
                </div>
            </form>
        </div>
    </div>
</section>