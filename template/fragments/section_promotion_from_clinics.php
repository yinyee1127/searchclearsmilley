<style>
  .w-100 {
    width: 100%;
  }

  .pt-8 {
    padding-top: 8px !important;
  }

  .pt-48 {
    padding-top: 48px !important;
  }

  .pb-8 {
    padding-bottom: 8px !important;
  }

  .pl-8 {
    padding-left: 8px !important;
  }

  .pr-8 {
    padding-right: 8px !important;
  }

  .mr-8 {
    margin-right: 8px !important;
  }

  .ml-8 {
    margin-left: 8px !important;
  }

  .pl-16 {
    padding-left: 16px !important;
  }

  .pr-16 {
    padding-right: 16px !important;
  }

  .pb-16 {
    padding-bottom: 16px !important;
  }

  .mr-16 {
    margin-right: 16px !important;
  }

  .ml-16 {
    margin-left: 16px !important;
  }

  .mb-16 {
    margin-bottom: 16px !important;
  }

  .pt-48 {
    padding-top: 48px !important;
  }

  .b-rd-4 {
    border-radius: 4px !important;
  }
</style>
<section class="site-section p-0 pt-48 element-animate">
  <div class="container p-0">
    <div class="row p-0 m-0">
      <div class="col p-0 m-0">
        <h2 class="text-section heading w-100 pt-48 pb-16">Promotion</h2>
      </div>
    </div>
    <div class="row p-0 m-0">
      <div class="col-md-10 col-lg-10 p-0 m-0">
        <div class="row p-0 m-0">
          <div class="col-6 col-sm-4 col-md-3 col-lg-3 p-0">
            <div class="media d-block ml-8 mr-8 mb-16">
              <a href="/clinic-age">
                <img class="feature-thumbnail" src="images/ads/promotion-edit-1.jpg" alt=" Featured Image">
              </a>
              <div class="media-body">
                <div style="font-size: 18px;margin-bottom: 4px; line-height: 22px; margin-top: 8px; font-weight: 600; color: rgba(0,0,0,0.8);">50% discount on consultation</div>
              </div>
            </div>
          </div>
          <div class="col-6 col-sm-4 col-md-3 col-lg-3 p-0">
            <div class="media d-block ml-8 mr-8 mb-16">
              <figure class="m-0">
                <a href="/clinic-page"><img src="images/ads/promotional-edit-5.jpg" alt="Image" class="img-fluid" style="border-radius:4px;"></a>
              </figure>
              <div class="media-body">
                <div style="font-size: 18px;margin-bottom: 4px; line-height: 22px; margin-top: 8px; font-weight: 600; color: rgba(0,0,0,0.8);">International Women's Day Special</div>
              </div>
            </div>
          </div>
          <div class="col-6 col-sm-4 col-md-3 col-lg-3 p-0">
            <div class="media d-block ml-8 mr-8 mb-16">
              <figure class="m-0">
                <a href="/clinic-page"><img src="images/ads/promotional-edit-8.png" alt="Image" class="img-fluid" style="border-radius:4px;"></a>
              </figure>
              <div class="media-body">
                <div style="font-size: 18px;margin-bottom: 4px; line-height: 22px; margin-top: 8px; font-weight: 600; color: rgba(0,0,0,0.8);">Free Cardiovascular Checkup</div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 p-0">
            <div class="media d-block ml-8 mr-8 mb-16">
              <figure class="m-0">
                <a href="/clinic-page">
                  <img src="images/ads/promotional-edit-6.jpg" alt="Image" class="img-fluid" style="border-radius:4px;">
                </a>
              </figure>
              <div class="media-body">
                <div style="font-size: 18px;margin-bottom: 4px; line-height: 22px; margin-top: 8px; font-weight: 600; color: rgba(0,0,0,0.8);">Voucher For Heart Screening</div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2 col-lg-2 p-0 m-0">
        <figure class="mb-0">
          <figcaption style="text-align:right; line-height:1.5rem; position:relative; top:-1.5rem;font-size: 14px;font-weight: 400;padding-bottom:4px;">Ad</figcaption>
          <img class="img-fluid" src="/images/ads/paid/ads-3-120x240.PNG" style="display:block; margin-left:auto; margin-right:0; position:relative; top:-1.5rem;margin-bottom:16px;max-width:125px;">
        </figure>
      </div>
    </div>
  </div>
</section>
