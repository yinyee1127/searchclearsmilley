<?php
$ads = [];
$featured_listing = [];

function from_xml_get_featured()
{
  global $dataset_path;
  $clinic_image_url = "/images/clinic2.jpg";
  $clinic_page_url = "/clinic-page";
  $xmlDoc = new DOMDocument();
  $xmlDoc->load($dataset_path);
  $record = $xmlDoc->getElementsByTagName('record');

  $result = [];
  for ($i = 0; $i < 8; $i++) {
    $clinic_name = $record->item($i)->getElementsByTagName('clinic_name');
    $city = $record->item($i)->getElementsByTagName('city');
    $website = $record->item($i)->getElementsByTagName('website');
    $result[$i]["clinic_name"] = $clinic_name->item(0)->nodeValue;
    $result[$i]["city"] = $city->item(0)->nodeValue;
    $result[$i]["website"] = $website->item(0)->nodeValue;
    $result[$i]["image_url"] = $clinic_image_url;
    $result[$i]["page_url"] = $clinic_page_url;
  }
  return $result;
}
?>

<style>
  .w-100 {
    width: 100%;
  }

  .pt-8 {
    padding-top: 8px !important;
  }

  .pt-48 {
    padding-top: 48px !important;
  }

  .pb-8 {
    padding-bottom: 8px !important;
  }

  .pl-8 {
    padding-left: 8px !important;
  }

  .pr-8 {
    padding-right: 8px !important;
  }

  .mr-8 {
    margin-right: 8px !important;
  }

  .ml-8 {
    margin-left: 8px !important;
  }

  .pl-16 {
    padding-left: 16px !important;
  }

  .pr-16 {
    padding-right: 16px !important;
  }

  .pb-16 {
    padding-bottom: 16px !important;
  }

  .mr-16 {
    margin-right: 16px !important;
  }

  .ml-16 {
    margin-left: 16px !important;
  }

  .mb-16 {
    margin-bottom: 16px !important;
  }

  .pt-48 {
    padding-top: 48px !important;
  }

  .b-rd-4 {
    border-radius: 4px !important;
  }
</style>
<section class="site-section p-0 pt-48 element-animate pb-0">
  <div class="container p-0">
    <div class="row p-0 m-0">
      <div class="col p-0 m-0">
        <figure class="mb-0">
          <figcaption style="text-align: center;line-height:1.5rem;position:relative;top:-1.5rem;font-size: 14px;font-weight: 400;padding-bottom:4px;">Advertisement</figcaption>
          <img class="img-fluid" src="/images/ads/paid/ads-5-728x90.PNG" style="display:block;/* margin-left:auto; *//* margin-right:0; */position:relative;top:-1.5rem;/* margin-bottom:16px; */height:90px;width: 728px;margin: 0 auto;">
        </figure>
      </div>
    </div>
</section>
<section class="site-section p-0 pt-48 element-animate pb-0">
  <div class="container p-0">
    <div class="row p-0 m-0">
      <div class="col p-0 m-0">
        <h2 class="text-section heading w-100 pt-48 pb-16"><?= $title ?></h2>
      </div>
    </div>
    <!-- featured clinic -->
    <div class="row p-0 m-0">
      <div class="col-md-10 p-0 m-0">
        <div class="row p-0 m-0">
          <?php
          $photos = [
            [
              "name" => "Bio Smile Dental Clinic",
              "photo" => "/images/ads/clinics/1_1_registration_counter.jpg",
              "opening_html" => '<span style="color: white;background: rgba(0, 0, 0, 0.7);padding: 10px;font-size: 12px;font-weight: 600;border-radius: 4px;letter-spacing: 1px;">Opens 8am, tomorrow</span>'
            ],
            [
              "name" => "Bala Dental Clinic",
              "photo" => "/images/ads/clinics/2_1.jpg",
              "opening_html" => '<span style="color: white;background: rgba(0, 0, 0, 0.7);padding: 10px;font-size: 12px;font-weight: 600;border-radius: 4px;letter-spacing: 1px;">Lunch time, reopening 2pm</span>'
            ],
            [
              "name" => "L&M Dental Clinic",
              "photo" => "/images/ads/clinics/3_1.png",
              "opening_html" => '<span style="color: white;background: rgba(0, 0, 0, 0.7);padding: 10px;font-size: 12px;font-weight: 600;border-radius: 4px;letter-spacing: 1px;">Closed. Opens 8am, 12/5</span>'
            ],
            [
              "name" => "Elementist Dentist",
              "photo" => "/images/ads/clinics/5_1.jpg",
              "opening_html" => '<span style="color: white;background: rgba(0, 0, 0, 0.7);padding: 10px;font-size: 12px;font-weight: 600;border-radius: 4px;letter-spacing: 1px;">Closing soon, 8pm</span>'
            ],
            [
              "name" => "Kidlinic",
              "photo" => "/images/ads/clinics/6_1.png",
              "opening_html" => '<span style="color: white;background: rgba(0, 0, 0, 0.7);padding: 10px;font-size: 12px;font-weight: 600;border-radius: 4px;letter-spacing: 1px;">Closed.</span>'
            ],
            [
              "name" => "Jaya Surya Dental",
              "photo" => "/images/ads/clinics/7_1.jpg",
              "opening_html" => '<span style="color: white;background: rgba(0, 0, 0, 0.7);padding: 10px;font-size: 12px;font-weight: 600;border-radius: 4px;letter-spacing: 1px;">Closed.</span>'
            ],
            [
              "name" => "White32 Whitening Centre",
              "photo" => "/images/ads/clinics/9_1.jpg",
              "opening_html" => '<span style="color: white;background: rgba(0, 0, 0, 0.7);padding: 10px;font-size: 12px;font-weight: 600;border-radius: 4px;letter-spacing: 1px;">Open for booking & walk-in</span>'
            ],
            [
              "name" => "Joice Dental Clinic",
              "photo" => "/images/ads/clinics/8_1.jpg",
              "opening_html" => "",
            ],
          ];

          $search = "";
          foreach (from_xml_get_featured() as $index => $record) {
            if ($index === 0) {
              $margin = 8;
            } else {
              $margin = 8;
            }

            // dd($photos[$index]["photo"]);

            $search =
              '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 p-0">
                <div class="media d-block ml-' . $margin . ' mr-8 mb-16">
                  <a href="' . $record["page_url"] . '">
                   <div class="image" style="position:relative;">
                      <img class="feature-thumbnail" src="' . $photos[$index]["photo"] . '" alt="Featured Image">
                      <div style="position: absolute;top: 155px;left: 0px;width: 100%;">
                        ' . (!empty($photos[$index]['opening_html']) ? $photos[$index]['opening_html'] : "") . '
                      </div>
                    </div>
                  </a>
                  <div class="media-body">
                    <div style="font-size: 18px;margin-bottom: 4px; line-height: 22px; margin-top: 8px; font-weight: 600; color: rgba(0,0,0,0.8);">' . $photos[$index]["name"] . '</div>
                    <div style="line-height: 0px;">
                      <i class="fa fa-map-marker padding10">' . $record["city"] . '</i>
                    </div>
                    <div style="line-height: 0px;margin-top: 8px;">
                      <span class="badge badge-pill badge-info" style="margin: 0;margin-bottom: 4px;border-radius: 4px;font-weight: 600;font-size:10px;">Maxillofacial </span>
                      <span class="badge badge-pill badge-info" style="margin: 0;margin-bottom: 4px;border-radius: 4px;font-weight: 600;font-size:10px;">Orthodontist</span>
                      <span class="badge badge-pill badge-info" style="margin: 0;margin-bottom: 4px;border-radius: 4px;font-weight: 600;font-size:10px;">Periodontist</span>
                      <span class="badge badge-pill badge-dark" style="margin: 0;margin-bottom: 4px;border-radius: 4px;font-weight: 600;font-size:10px;">GP</span>
                      <span class="badge badge-pill badge-dark" style="margin: 0;margin-bottom: 4px;border-radius: 4px;font-weight: 600;font-size:10px;">Teledental</span>
                    </div>        
                  </div>
              </div>
            </div>';
            // Insert data into Array
            $array[] = $search;
          }
          foreach ($array as $value) {
            echo $value;
          }
          ?>
        </div>
        <div class="horizontal-scroll-right" style="
    box-shadow: 0 8px 12px 0 rgba(44,44,45,.27), 0 0 0 1px rgba(44,44,45,.07);
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 40px;
    height: 40px;
    cursor: pointer;
    background-color: #fff;
    border-radius: 50%;
    opacity: .8;
    right: -15px !important;
    top: 300px;
"><svg height="32" width="32" viewBox="0 0 24 24" fill="#57585a" xmlns="http://www.w3.org/2000/svg">
            <path d="M9.29 15.88L13.17 12 9.29 8.12c-.39-.39-.39-1.02 0-1.41.39-.39 1.02-.39 1.41 0l4.59 4.59c.39.39.39 1.02 0 1.41L10.7 17.3c-.39.39-1.02.39-1.41 0-.38-.39-.39-1.03 0-1.42z" id="iconArrow"></path>
          </svg></div>
        <div class="horizontal-scroll-left" style="
    box-shadow: 0 8px 12px 0 rgba(44,44,45,.27), 0 0 0 1px rgba(44,44,45,.07);
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 40px;
    height: 40px;
    cursor: pointer;
    background-color: #fff;
    border-radius: 50%;
    opacity: .8;
    left: -15px !important;
    top: 300px;
"><svg height="32" width="32" viewBox="0 0 24 24" fill="#57585a" xmlns="http://www.w3.org/2000/svg" style="-webkit-transform: rotate(180deg); transform: rotate(180deg);">
            <path d="M9.29 15.88L13.17 12 9.29 8.12c-.39-.39-.39-1.02 0-1.41.39-.39 1.02-.39 1.41 0l4.59 4.59c.39.39.39 1.02 0 1.41L10.7 17.3c-.39.39-1.02.39-1.41 0-.38-.39-.39-1.03 0-1.42z" id="iconArrowLeft"></path>
          </svg></div>
      </div>
      <div class="col-md-2 col-lg-2 p-0 m-0">
        <figure class="mb-0">
          <figcaption style="text-align:right; line-height:1.5rem; position:relative; top:-1.5rem;font-size: 14px;font-weight: 400;padding-bottom:4px;">Ad</figcaption>
          <img class="img-fluid" src="/images/ads/paid/ads-120x240.jpg" style="display:block; margin-left:auto; margin-right:0; position:relative; top:-1.5rem;margin-bottom:16px;max-width:120px;">
        </figure>
        <figure class="mb-0">
          <figcaption style="text-align:right; line-height:1.5rem; position:relative; top:-1.5rem;font-size: 14px;font-weight: 400;padding-bottom:4px;">Ad</figcaption>
          <img class="img-fluid" src="/images/ads/paid/ads-2-120x400.PNG" style="display:block; margin-left:auto; margin-right:0; position:relative; top:-1.5rem;margin-bottom:16px;max-width:120px;">
        </figure>
      </div>
    </div>
</section>