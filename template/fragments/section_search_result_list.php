<?php

use Illuminate\Database\Capsule\Manager as Capsule;

function fetch_from_xml($dataset_path) {
  $xmlDoc = new DOMDocument();
  $xmlDoc->load($dataset_path);

  $record = $xmlDoc->getElementsByTagName('record');

  // get q paramenter from URI
  $name = (isset($_POST["doctor_name"]) and !empty($_POST["doctor_name"])) ? $_POST["doctor_name"] : "0";
  $loc = (isset($_POST["location"]) and !empty($_POST["location"])) ? $_POST["location"] : "0";
  $clinic = (isset($_POST["clinic_name"]) and !empty($_POST["clinic_name"])) ? $_POST["clinic_name"] : "0";
  $treatment = (isset($_POST["treatment_option"]) and !empty($_POST["treatment_option"])) ? $_POST["treatment_option"] : "0";

  // an empty array for the data 
  $array = [];

  // $result = $xmlDoc->getElementsByTagName('record')->item(0)->nodeValue;
  // $value = $xmlDoc->getElementsByTagName('record')->item(0)->getElementsByTagName('city')->item(0)->nodeValue;

  if (strlen($name) > 0 || strlen($loc) > 0) {
    $search = "";
    for ($i = 0; $i < 1000; $i++) {
      $clinic_name = $record->item($i)->getElementsByTagName('clinic_name');
      $city = $record->item($i)->getElementsByTagName('city');
      $email = $record->item($i)->getElementsByTagName('email');
      $city = $record->item($i)->getElementsByTagName('city');
      $postal_code = $record->item($i)->getElementsByTagName('postal_code');
      $clinic_no = $record->item($i)->getElementsByTagName('clinic_no');
      $created_date = $record->item($i)->getElementsByTagName('created_date');
      $website = $record->item($i)->getElementsByTagName('website');
      $thumbnail = $record->item($i)->getElementsByTagName('thumbnail');
      $language = $record->item($i)->getElementsByTagName('language');
      if ($clinic_name->item(0)->nodeType == 1) {
        if (stristr($clinic_name->item(0)->nodeValue, $name) && stristr($city->item(0)->nodeValue, $loc)) {
          $clinic_image = "/images/clinic2.jpg";
          $class_heart = "font-size:20px;color:red;float:right";
          $price = "RM50-Rm100";

          $search =
          '
          <div class="image" style="background-image: url(\'' . $clinic_image . '\');max-width:283px;max-height:222px"></div>
          <div class="text" style="padding:0px;width:100%;margin:12px">
              <h2 class="heading" style="font-size:20px">' . $clinic_name->item(0)->nodeValue .
                '<span><i class="fa fa-heart" style="' . $class_heart . '"></i></span>
              </h2>
              <i class="fa fa-map-marker padding10">' . $city->item(0)->nodeValue . '</i><br>
              <i class="fa fa-phone padding10">Contact No: ' . $clinic_no->item(0)->nodeValue . '</i><br>
              <i class="fa fa-envelope padding10">Email Address : ' . $email->item(0)->nodeValue . '</i><br>
              <span class="meta"><i class="fa fa-money padding10">' . $price . '</i></span>
                  <input type="submit" style="float:right" class="search-submit btn btn-primary" value="Book Appointment">
              </span>
          </div>
          ';

          // Insert data into Array
          $array[] = $search;
        } elseif ($loc == "0" && stristr($clinic_name->item(0)->nodeValue, $name)) {
          $clinic_image = "/images/clinic2.jpg";
          $class_heart = "font-size:20px;color:red;float:right";
          $price = "RM50-Rm100";

          $search =
            '
          <div class="image" style="background-image: url(\'' . $clinic_image . '\');max-width:283px;max-height:222px"></div>
          <div class="text" style="padding:0px;width:100%;margin:12px">
              <h2 class="heading" style="font-size:20px">' . $clinic_name->item(0)->nodeValue .
                '<span><i class="fa fa-heart" style="' . $class_heart . '"></i></span>
              </h2>
              <i class="fa fa-map-marker padding10">' . $city->item(0)->nodeValue . '</i><br>
              <i class="fa fa-phone padding10">Contact No: ' . $clinic_no->item(0)->nodeValue . '</i><br>
              <i class="fa fa-envelope padding10">Email Address : ' . $email->item(0)->nodeValue . '</i><br>
              <span class="meta"><i class="fa fa-money padding10">' . $price . '</i></span>
                  <input type="submit" style="float:right" class="search-submit btn btn-primary" value="Book Appointment">
              </span>
          </div>
          ';

          // Insert data into Array
          $array[] = $search;
        } elseif ($name == "0" && stristr($city->item(0)->nodeValue, $loc)) {
          $clinic_image = "/images/clinic2.jpg";
          $class_heart = "font-size:20px;color:red;float:right";
          $price = "RM50-Rm100";

          $search =
            '
          <div class="image" style="background-image: url(\'' . $clinic_image . '\');max-width:283px;max-height:222px"></div>
          <div class="text" style="padding:0px;width:100%;margin:12px">
              <h2 class="heading" style="font-size:20px">' . $clinic_name->item(0)->nodeValue .
                '<span><i class="fa fa-heart" style="' . $class_heart . '"></i></span>
              </h2>
              <i class="fa fa-map-marker padding10">' . $city->item(0)->nodeValue . '</i><br>
              <i class="fa fa-phone padding10">Contact No: ' . $clinic_no->item(0)->nodeValue . '</i><br>
              <i class="fa fa-envelope padding10">Email Address : ' . $email->item(0)->nodeValue . '</i><br>
              <span class="meta"><i class="fa fa-money padding10">' . $price . '</i></span>
                  <input type="submit" style="float:right" class="search-submit btn btn-primary" value="Book Appointment">
              </span>
          </div>
          ';

          // Insert data into Array
          $array[] = $search;
        }
      }
    }
  }
  return [
    "array" => $array,
    "loc" => $loc,
    "name" => $name,
    "clinic" => $clinic,
    "treatment" => $treatment,
  ];
}

function fetch_from_database()
{
  // get q paramenter from URI
  $name = (isset($_POST["doctor_name"]) and !empty($_POST["doctor_name"])) ? $_POST["doctor_name"] : "";
  $loc = (isset($_POST["location"]) and !empty($_POST["location"])) ? $_POST["location"] : "";
  $clinic = (isset($_POST["clinic_name"]) and !empty($_POST["clinic_name"])) ? $_POST["clinic_name"] : "";
  $treatment = (isset($_POST["treatment_option"]) and !empty($_POST["treatment_option"])) ? $_POST["treatment_option"] : "";

  $results = Capsule
    ::table("clinics")
    ->whereRaw(Capsule::raw("clinic_name LIKE \"%{$clinic}%\""))
    ->whereRaw(Capsule::raw("practicising_address LIKE \"%{$loc}%\""))
    ->get();
  $array = [];

  $search = '';
  foreach($results as $result) {

    //$result->{"id"}
    //$result->{"clinic_name"}
    //$result->{"practicising_address"}
    //$result->{"lang"}
    //$result->{"address"}
    //$result->{"state"}
    //$result->{"city"}
    //$result->{"postcode"}
    //$result->{"clinic_hash"}
    //$result->{"address_hash"}
    //$result->{"created_at"}
    //$result->{"updated_at"}

    $clinic_name = $result->{"clinic_name"};
    $city = $result->{"city"};
    $email = "[NOT AVAILABLE]";
    $postal_code = $result->{"postcode"};
    $clinic_no = "[NOT AVAILABLE]";
    $created_date = $result->{"created_at"};
    $website = "[NOT AVAILABLE]";
    $thumbnail = "https://via.placeholder.com/500/000000/FFFFFF/?text=Clinic%20Thumbnail%20Not%20Available";
    $language = $result->{"lang"};

    $clinic_image = "https://via.placeholder.com/500/000000/FFFFFF/?text=Clinic%20Thumbnail%20Not%20Available";
    $class_heart = "font-size:20px;color:red;float:right";
    $price = "[PRICE NOT AVAILABLE]";

    $search =
         '<div class="image" style="background-image: url(\'' . $clinic_image . '\');max-width:283px;max-height:222px"></div>
          <div class="text" style="padding:0px;width:100%;margin:12px">
              <h2 class="heading" style="font-size:20px">' . $clinic_name .
      '<span><i class="fa fa-heart" style="' . $class_heart . '"></i></span>
              </h2>
              <i class="fa fa-map-marker padding10">' . $city . '</i><br>
              <i class="fa fa-phone padding10">Contact No: ' . $clinic_no . '</i><br>
              <i class="fa fa-envelope padding10">Email Address : ' . $email . '</i><br>
              <span class="meta"><i class="fa fa-money padding10">' . $price . '</i></span>
                  <input type="submit" style="float:right" class="search-submit btn btn-primary" value="Book Appointment">
              </span>
          </div>
          ';
    // Insert data into Array
    $array[] = $search;
  }
  return [
    "array" => $array,
    "loc" => $loc,
    "name" => $name,
    "clinic" => $clinic,
    "treatment" => $treatment,
  ];
}

global $dataset_path;
// $result = fetch_from_xml($dataset_path);
$result = fetch_from_database();
$array = $result["array"];
$loc = $result["loc"];
$name = $result["name"];
$clinic = $result["clinic"];
$treatment = $result["treatment"];
?>

<section class="site-section pt-3 element-animate">
    <div class="container">
        <!-- START SEARCH RESULT HEADER -->
        <div class="row element-animate">
            <div class="col-md-7 text-center section-heading">
                <h2 class="text-primary heading" style="font-size:28px;text-align:left">Dental Clinics in <?=
                $loc ?></h2>
                <p style="float:left">Book appointment here.</p>
            </div>
        </div>
        <!-- END SEARCH RESULT HEADER -->

        <!-- START SEARCH RESULT LISITNG -->
        <?php
        // $rpp = result_per_page
        $rpp = 20;
        $number_of_pages = ceil(count($array)/$rpp);

        if(isset($_GET['page'])) {
          $page = $_GET['page'];
        } else {
          $page = "1";
        }

        $pg = $page;
        $page_array = [];
        // Display Page
        for ($pg=1;$pg<=$number_of_pages;$pg++) {
            //echo '<a href="test-pag.php?q='. $name .'&page='. $pg .'">'.$pg.' - </a>';
            $page_array[] = $pg;
        }
        
        foreach ($array as $index => $value) {
            if ($index>=$rpp*($page-1) && $index<$rpp*$page) {
?>
            <div class="block-19">
              <div row style="padding-bottom:20px">
                <div class="block-3 d-md-flex"><?= $value;?></div>
              </div>
            </div>
<?php
            } 
        }
        if(empty($array)) {
            echo "
            <div class='col-md-12 col-lg-12 mb-5'>
              <h1 style='font-style:Rubik; padding-top:20px'>Sorry, Dentist not found!</h1>
              <p>Click <a href='#'>here</a> to back to previous page.</p>
            </div>";
        }
        ?>
    <!-- END SEARCH RESULT LISITNG -->

      <!-- START PAGINATION -->
      <div class="row mb-5">
        <div class="col-md-12 text-center">
          <div class="block-27">
            <ul>
<?php
            if($page>3){
              echo '<li><a href="search-result?page='. ($page-1) .'">&lt;</a></li>';
            }
            foreach ($page_array as $index => $pg) { 
              if($pg==$page){
                echo '<li class="active"><a href="search-result?page='. $pg .'">'. $pg .'</a></li>';
              } elseif ( $page>0 && $page<4 && $pg<6 || $page>3 && $pg<$page+3 && $pg>$page-3 ){
                echo '<li><a href="search-result?page='. $pg .'">'. $pg .'</a></li>';
              }
            }
            if($page<count($page_array)-3){
              echo '<li><a href="search-result?page='. ($page+1) .'">&gt;</a></li>';
            }
?>
              
            </ul>
          </div>
        </div>
      </div>
      <!-- END PAGINATION -->
    </div>
</section>