<header role="banner">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <img style="max-width:150px; height:auto;"src="images/logo/color.png" alt="Image placeholder" class="img-fluid">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
        <ul class="navbar-nav mx-auto" style="margin-right:200px !important">
          <li>
            <a class="nav-link <?= $nav_active_home_page ?>" href="<?= $url_home_page ?>">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Services</a>
            <div class="dropdown-menu" aria-labelledby="dropdown04">
              <a class="dropdown-item" href="courses.html">Promotion</a>
              <a class="dropdown-item" href="courses.html">Teledental Services</a>
              <a class="dropdown-item" href="courses.html">Events</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link <?= $nav_active_treaments_page ?>" href="<?= $url_treatments_page ?>">Treatments</a>
          </li>
          <li class="nav-item">
            <a class="nav-link <?= $nav_active_contact_us_page ?>" href="<?= $url_contact_us_page ?>">Contact Us</a>
          </li>
        </ul>
        <ul class="navbar-nav absolute-right">
          <li>
            <a href="<?= $url_login_page ?>">Login</a> / <a href="<?= $url_register_page ?>">Register</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>
