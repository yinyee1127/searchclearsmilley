<section class="site-section element-animate pt-0 pb-0">
  <div class="container p-0">
    <div class="row p-0 m-0">
      <div class="col p-0 m-0">
        <h2 class="text-section heading w-100 pt-48 pb-16">Teledental Services</h2>
      </div>
    </div>
    <div class="row p-0 m-0">
      <div class="col-md-10 col-lg-10 p-0 m-0">
        <div class="row p-0 m-0">
          <div class="col-6 col-sm-4 col-md-3 col-lg-3 p-0">
            <div class="media d-block ml-8 mr-8 mb-16">
              <figure class="m-0">
                <a href="/clinic-page">
                  <img src="images/exdentist1.jpg" alt="Image" class="img-fluid" style="border-radius:4px;">
                </a>
              </figure>
              <div class="media-body">
                <span>What is Teledental Services</span>
              </div>
            </div>
          </div>
          <div class="col-6 col-sm-4 col-md-3 col-lg-3 p-0">
            <div class="media d-block ml-8 mr-8 mb-16">
              <figure class="m-0">
                <a href="/clinic-page">
                  <img src="images/exdentist2.jpg" alt="Image" class="img-fluid" style="border-radius:4px;">
                </a>
              </figure>
              <div class="media-body">
                <span>SOP</span>
              </div>
            </div>
          </div>
          <div class="col-6 col-sm-4 col-md-3 col-lg-3 p-0">
            <div class="media d-block ml-8 mr-8 mb-16">
              <figure class="m-0">
                <a href="/clinic-page">
                  <img src="images/exdentist3.jpg" alt="Image" class="img-fluid" style="border-radius:4px;">
                </a>
              </figure>
              <div class="media-body">
                <span>Treatment Offers</span>
              </div>
            </div>
          </div>
          <div class="col-6 col-sm-4 col-md-3 col-lg-3 p-0">
            <div class="media d-block ml-8 mr-8 mb-16">
              <figure class="m-0">
                <a href="/clinic-page">
                  <img src="images/exdentist4.jpg" alt="Image" class="img-fluid" style="border-radius:4px;">
                </a>
              </figure>
              <div class="media-body">
                <span>Panel of Doctors</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2 col-lg-2 p-0 m-0">
        <figure class="mb-0">
          <figcaption style="text-align:right; line-height:1.5rem; position:relative; top:-1.5rem;font-size: 14px;font-weight: 400;padding-bottom:4px;">Ad</figcaption>
          <img class="img-fluid" src="/images/ads/paid/ads-4-120x400.PNG" style="display:block; margin-left:auto; margin-right:0; position:relative; top:-1.5rem;margin-bottom:16px;max-width:120px;">
        </figure>
      </div>
    </div>
  </div>
</section>
<section class="site-section p-0 pt-48 element-animate pb-0">
  <div class="container p-0">
    <div class="row p-0 m-0">
      <div class="col p-0 m-0">
        <figure class="mb-0">
          <figcaption style="text-align: center;line-height:1.5rem;position:relative;top:-1.5rem;font-size: 14px;font-weight: 400;padding-bottom:4px;">Advertisement</figcaption>
          <img class="img-fluid" src="/images/ads/paid/ads-5-728x90.PNG" style="display:block;/* margin-left:auto; *//* margin-right:0; */position:relative;top:-1.5rem;/* margin-bottom:16px; */height:90px;width: 728px;margin: 0 auto;">
        </figure>
      </div>
    </div>
</section>