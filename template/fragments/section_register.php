<?php
// include('../template/pages/controller.php');

use Hybridauth\Hybridauth;

global $CONFIG_SOCIAL2;
$hybridauth = new Hybridauth($CONFIG_SOCIAL2);
$adapters = $hybridauth->getConnectedAdapters();
?>
<section class="site-section" style="background-image: url(images/background/bg5.jpg);background-size:cover">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-7">
        <div class="form-wrap register">
        <h2 class="h2 register">Register Now!</h2>
        <h4 class="h4 register">You can enjoy more features by register into our website.</h4><br>
          <form action="/register" method="POST">
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="name">Full Name:</label>
                <input type="text" id="name" name="f_name" class="form-control register py-2">
              </div>
              <div class="col-md-6 form-group">
                <label for="name">IC/Passport No:</label>
                <input type="text" id="name" name="ic" class="form-control register py-2">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <label for="name">Address</label>
                <input type="text" id="name" name="address" class="form-control register py-2">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 form-group">
                <label for="name">Contact No:</label>
                <input type="text" id="name" name="contact" class="form-control register py-2">
              </div>
              <div class="col-md-6 form-group">
                <label for="name">Email Address:</label>
                <input type="text" id="name" name="email" class="form-control register py-2">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <label for="name">Password:</label>
                <input type="password" id="name" name="password" class="form-control register py-2 ">
              </div>
            </div>
            <div class="row mb-5">
              <div class="col-md-12 form-group">
                <label for="name">Re-type Password:</label>
                <input type="password" id="name" name="retype-password" class="form-control register py-2">
              </div>
            </div>
            <div class="row mb-2">
              <div class="col-md-12 form-group">
                <input type="checkbox" id="remember" name="remember" value="signin">
                <label for="remember"> I agree with <a href="#">Terms and Conditions.</a></label><br>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <input type="submit" name="register" value="Register" class="btn btn-primary px-5 py-2 button login">
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">                    
                <div class="text-center">Or</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">                    
                <a href="<?= $CONFIG_SOCIAL2["callback"]; ?>?provider=Google" class="btn btn-primary px-5 py-2 button login" role="button"><i class="fa fa-google padding10 text-center" ></i> Continue with Google</a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 form-group">
                <a href="<?= $CONFIG_SOCIAL2["callback"]; ?>?provider=Facebook" class="btn btn-primary px-5 py-2 button login" role="button"><i class="fa fa-facebook padding10 text-center" ></i> Continue with Facebook</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>