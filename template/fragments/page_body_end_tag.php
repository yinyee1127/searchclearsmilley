<!-- loader -->
<div id="loader" class="show fullscreen">
    <svg class="circular" width="48px" height="48px">
        <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
        <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214" />
    </svg>
</div>
<script src="js/jquery-3.2.1.min.js"></script>

<!-- jQuery UI -->
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

<script src="js/jquery-migrate-3.0.0.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>

<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<!-- ONLY FOR CONTACT US PAGE -->
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="js/google-map.js"></script> -->
<!-- /END -->
<script src="js/main.js"></script>

<script>
    // Autocomplete
    $(function() {

        $clinic_name = $("form.form1 .form-control[name=clinic_name]");
        $doctor_name = $("form.form1 .form-control[name=doctor_name]");
        $location = $("form.form1 .form-control[name=location]");
        $treatment_option = $("form.form1 .form-control[name=treatment_option]");
        $clinic_name2 = $("form.form2 .form-control[name=clinic_name]");
        $doctor_name2 = $("form.form2 .form-control[name=doctor_name]");
        $location2 = $("form.form2 .form-control[name=location]");
        $treatment_option2 = $("form.form2 .form-control[name=treatment_option]");

        var autocomplete_config = function(selector, query) {
            return {
                source: function(request, response) {
                    // Fetch data
                    $.ajax({
                        url: "<?= $url_search_autocomplete ?>",
                        type: 'post',
                        dataType: "json",
                        data: {
                            value: request.term,
                            query: query
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                },
                select: function(event, ui) {
                    // Set selection
                    selector.val(ui.item.label); // display the selected text
                    return false;
                }
            };
        };
        // Autocomplete for Name
        $doctor_name.autocomplete(
            autocomplete_config($doctor_name, "doctor_name")
        );
        // Autocomplete for Clinic Name
        $clinic_name.autocomplete(
            autocomplete_config($clinic_name, "clinic_name")
        );
        // Autocomplete for Location
        $location.autocomplete(
            autocomplete_config($location, "location")
        );
        // Autocomplete for Treatment options
        $treatment_option.autocomplete(
            autocomplete_config($treatment_option, "treatment_option")
        );

        // Autocomplete for Name
        $doctor_name2.autocomplete(
            autocomplete_config($doctor_name2, "doctor_name")
        );
        // Autocomplete for Clinic Name
        $clinic_name2.autocomplete(
            autocomplete_config($clinic_name2, "clinic_name")
        );
        // Autocomplete for Location
        $location2.autocomplete(
            autocomplete_config($location2, "location")
        );
        // Autocomplete for Treatment options
        $treatment_option2.autocomplete(
            autocomplete_config($treatment_option2, "treatment_option")
        );

        jQuery('#toggle-demo').bootstrapToggle('on')
    });

    function split(val) {
        return val.split(/,\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }
</script>