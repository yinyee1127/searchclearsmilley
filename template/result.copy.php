<?php
  $xmlDoc = new DOMDocument();
  $xmlDoc -> load("../dataset.xml");

  $record = $xmlDoc->getElementsByTagName('record');

  // get q paramenter from URI
  $name=$_GET["name"];
  $loc=$_GET["loc"];
  $clinic=$_GET["clinic"];
  $treatment=$_GET["treatment"];
  if(""==$name){ $name = "0"; } 
  if(""==$loc){ $loc = "0"; } 
  if(""==$clinic){ $clinic = "0"; } 
  if(""==$treatment){ $treatment = "0"; }
  

  // an empty array for the data 
  $array = [];

  // $result = $xmlDoc->getElementsByTagName('record')->item(0)->nodeValue;
  // $value = $xmlDoc->getElementsByTagName('record')->item(0)->getElementsByTagName('city')->item(0)->nodeValue;

  if(strlen($name)>0||strlen($loc)>0){
    $search = "";
    for($i=0; $i<1000; $i++){
      $clinic_name=$record->item($i)->getElementsByTagName('clinic_name');
      $city=$record->item($i)->getElementsByTagName('city');
      $email=$record->item($i)->getElementsByTagName('email');
      $city=$record->item($i)->getElementsByTagName('city');
      $postal_code=$record->item($i)->getElementsByTagName('postal_code');
      $clinic_no=$record->item($i)->getElementsByTagName('clinic_no');
      $created_date=$record->item($i)->getElementsByTagName('created_date');
      $website=$record->item($i)->getElementsByTagName('website');
      $thumbnail=$record->item($i)->getElementsByTagName('thumbnail');
      $language=$record->item($i)->getElementsByTagName('language');
      if($clinic_name->item(0)->nodeType==1){
        if(stristr($clinic_name->item(0)->nodeValue,$name) && stristr($city->item(0)->nodeValue,$loc)){
          $search = 
          '
          <div class="image" style="background-image: url(\'../images/clinic2.jpg\');max-width:283px;max-height:222px"></div>

          <div class="text" style="padding:0px;width:100%;margin:12px">
            <h2 class="heading" style="font-size:20px">'. $clinic_name->item(0)->nodeValue .'<span><i class="fa fa-heart-o" style="font-size:20px;color:red;float:right;"></i></span></h2>
            <i class="fa fa-map-marker padding10">'. $city->item(0)->nodeValue .'</i><br>
            <i class="fa fa-phone padding10">Contact No: '. $clinic_no->item(0)->nodeValue .'</i><br>
            <i class="fa fa-envelope padding10">Email Address : '. $email->item(0)->nodeValue .'</i><br>
            <span class="meta"><i class="fa fa-money padding10">RM50-Rm100</i></span><input type="submit" style="float:right" class="search-submit btn btn-primary" value="Book Appointment" ></span>
          </div>
          ';

          // Insert data into Array
          $array[] = $search;

        } elseif($loc == "0" && stristr($clinic_name->item(0)->nodeValue,$name)){
          $search = 
          $search = 
          '
          <div class="image" style="background-image: url(\'../images/clinic2.jpg\');max-width:283px;max-height:222px"></div>

          <div class="text" style="padding:0px;width:100%;margin:12px">
            <h2 class="heading" style="font-size:20px">'. $clinic_name->item(0)->nodeValue .'<span><i class="fa fa-heart-o" style="font-size:20px;color:red;float:right;"></i></span></h2>
            <i class="fa fa-map-marker padding10">'. $city->item(0)->nodeValue .'</i><br>
            <i class="fa fa-phone padding10">Contact No: '. $clinic_no->item(0)->nodeValue .'</i><br>
            <i class="fa fa-envelope padding10">Email Address : '. $email->item(0)->nodeValue .'</i><br>
            <span class="meta"><i class="fa fa-money padding10">RM50-Rm100</i></span><input type="submit" style="float:right" class="search-submit btn btn-primary" value="Book Appointment" ></span>
          </div>
          ';

          // Insert data into Array
          $array[] = $search;

        } elseif($name == "0" && stristr($city->item(0)->nodeValue,$loc)){
          $search = 
          '
          <div class="image" style="background-image: url(\'../images/clinic2.jpg\');max-width:283px;max-height:222px"></div>

          <div class="text" style="padding:0px;width:100%;margin:12px">
            <h2 class="heading" style="font-size:20px">'. $clinic_name->item(0)->nodeValue .'<span><i class="fa fa-heart-o" style="font-size:20px;color:red;float:right;"></i></span></h2>
            <i class="fa fa-map-marker padding10">'. $city->item(0)->nodeValue .'</i><br>
            <i class="fa fa-phone padding10">Contact No: '. $clinic_no->item(0)->nodeValue .'</i><br>
            <i class="fa fa-envelope padding10">Email Address : '. $email->item(0)->nodeValue .'</i><br>
            <span class="meta"><i class="fa fa-money padding10">RM50-Rm100</i></span><input type="submit" style="float:right" class="search-submit btn btn-primary" value="Book Appointment" ></span>
          </div>
          ';

          // Insert data into Array
          $array[] = $search;

        }
      }
    }
  }

  // $rpp = result_per_page
    $rpp = 20;
    $number_of_pages = ceil(count($array)/$rpp);

    if (!isset($_GET['page'])) {
      $page = 1;
    } else {
      $page = $_GET['page'];
    }
?>

<!doctype html>
<html lang="en">
  <head>
    <title>Findentist</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <link rel="stylesheet" href="../css/bootstrap.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/owl.carousel.min.css">

    <link rel="stylesheet" href="../fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="../fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="../css/magnific-popup.css">
    
    <!-- Scrollbar added on Autocomplete -->
    <style>.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden;}</style>

    <!-- Theme Style -->
    <link rel="stylesheet" href="../css/style.css">

    <!-- Script -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script>
      // Autocomplete
        $( function() {

          // Autocomplete for Name
          $( "#name" ).autocomplete({
          source: function( request, response ) {
            // Fetch data
            $.ajax({
            url: "search.php",
            type: 'post',
            dataType: "json",
            data: {
              name: request.term
            },
            success: function( data ) {
              response( data );
            }
            });
          },
          select: function (event, ui) {
            // Set selection
            $('#name').val(ui.item.label); // display the selected text
            return false;
          }
          });

          // Autocomplete for Clinic
          $( "#clinic" ).autocomplete({
          source: function( request, response ) {
            // Fetch data
            $.ajax({
            url: "search.php",
            type: 'post',
            dataType: "json",
            data: {
              clinic: request.term
            },
            success: function( data ) {
              response( data );
            }
            });
          },
          select: function (event, ui) {
            // Set selection
            $('#clinic').val(ui.item.label); // display the selected text
            return false;
          }
          });

          // Autocomplete for Location
          $( "#loc" ).autocomplete({
          source: function( request, response ) {
            // Fetch data
            $.ajax({
            url: "search.php",
            type: 'post',
            dataType: "json",
            data: {
              loc: request.term
            },
            success: function( data ) {
              response( data );
            }
            });
          },
          select: function (event, ui) {
            // Set selection
            $('#loc').val(ui.item.label); // display the selected text
            return false;
          }
          });

          // Autocomplete for Treatment
          $( "#treatment" ).autocomplete({
          source: function( request, response ) {
            // Fetch data
            $.ajax({
            url: "search.php",
            type: 'post',
            dataType: "json",
            data: {
              treatment: request.term
            },
            success: function( data ) {
              response( data );
            }
            });
          },
          select: function (event, ui) {
            // Set selection
            $('#treatment').val(ui.item.label); // display the selected text
            return false;
          }
          });
        });
        function split( val ) {
          return val.split( /,\s*/ );
        }
        function extractLast( term ) {
          return split( term ).pop();
        }
    </script>
  </head>

  <body>
    <header role="banner">
     
      <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
          <img style=" max-width:150px; height:auto;"src="../images/logo/color.png" alt="Image placeholder" class="img-fluid">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
            <ul class="navbar-nav mx-auto" style="margin-right:200px !important">
              <li>
                <a class="nav-link active" href="../html/home.html">Home</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../html/about.html">About Us</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="../html/contact.html">Contact Us</a>
              </li>
            </ul>
            <ul class="navbar-nav absolute-right">
              <li>
                <a href="../html/login.html">Login</a> / <a href="../html/register.html">Register</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>
    <div class="block-17" style="margin:20px">
      <form action="result.php" autocomplete="off" class="d-block d-lg-flex">
        <div class="fields d-block d-lg-flex">
          <div class="textfield-search one-third "><input type="text" name="loc" class="form-control" placeholder="Search by location..." value="<?php if($loc=="0"){echo "";} else { echo $loc; } ?>"></div>
          <div class="textfield-search one-third "><input type="text" name="clinic" class="form-control" placeholder="Search by clinic name..." value="<?php if($clinic=="0"){echo "";} else { echo $clinic; } ?>"></div>
          <div class="textfield-search one-third "><input type="text" name="name" class="form-control" placeholder="Search by doctor name..." value="<?php if($name=="0"){echo "";} else { echo $name; } ?>"></div>
          <div class="textfield-search one-third "><input type="text" name="treatment" class="form-control" placeholder="Search by treatment..." value="<?php if($treatment=="0"){echo "";} else { echo $treatment; } ?>"></div>
        </div>
        <input type="submit" class="search-submit btn btn-primary" value="Search" >
      </form>
    </div>

    <section class="site-section pt-3 element-animate">
      <div class="container" style="margin-right:inherit;margin-left:inherit">
        <div class="row element-animate">
          <div class="col-md-7 text-center section-heading">
            <h2 class="text-primary heading" style="font-size:28px;text-align:left">Dental Clinics in Mutiara Damansara</h2>
            <p style="float:left">Book appointment here.</p>
          </div>
        </div>
    <!-- The Real Result -->
<?php
  // echo "Starting number: ". $this_page_first_result = $page ."<br><br>";
  if($array != null){
    foreach ($array as $index => $value){
      if ($index>=$rpp*($page-1) && $index<$rpp*$page) {
        ?>
        <div class="block-19">
          <div row style="padding-bottom:20px">
            <div class="block-3 d-md-flex">
        <?php
        echo $value;
        ?>
            </div>
          </div>
        </div>
        <?php
      } 
    }
  } else {
    echo "
    <div class='col-md-12 col-lg-12 mb-5'>
      <h1 style='font-style:Rubik; padding-top:20px'>Sorry, Dentist not found!</h1>
      <p>Click <a href=index.html>here</a> to back to previous page.</p>
    </div>"
    ;
  }

  $page_array = [];

  // Display Page
  for ($page=1;$page<=$number_of_pages;$page++){
    //echo '<a href="test-pag.php?q='. $name .'&page='. $page .'">'.$page.' - </a>';
    $page_array[] = $page;
  }
?>
        <!-- The Real Result end here -->
      </div>
      <!-- Page Pagination -->
      <div class="row mb-5">
        <div class="col-md-12 text-center">
          <div class="block-27">
            <ul>
              <li><a href="result.php?name=<?php echo $name; ?>&loc=<?php echo $loc?>&page=<?php echo $_GET['page']-1; ?>">&lt;</a></li>
              <!-- <li class="active"><span>1</span></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li> -->
<?php
              if(!isset($_GET['page'])){ $page = 1; } else { $page = $_GET['page']; }
              foreach ($page_array as $index => $pg) { 
                if($pg==$page){
?>
                  <li class="active"><a href="result.php?name=<?php echo $name; ?>&loc=<?php echo $loc?>&page=<?php echo $pg; ?>"><?php echo $pg; ?></a></li>
<?php
                } else {
?>
                  <li><a href="result.php?name=<?php echo $name; ?>&loc=<?php echo $loc?>&page=<?php echo $pg; ?>"><?php echo $pg; ?></a></li>
<?php
                }
              }
?>
              <li><a href="result.php?name=<?php echo $name; ?>&loc=<?php echo $loc?>&page=<?php echo $_GET['page']+1; ?>">&gt;</a></li>
            </ul>
          </div>
        </div>
      </div>
    </section>
<!-- footer -->
  <div class="py-5 block-22">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md-6 mb-4 mb-md-0 pr-md-5">
          <h2 class="heading">Subscribe to Us!</h2>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi accusantium optio und.</p>
        </div>
        <div class="col-md-6">
          <form action="#" class="subscribe">
            <div class="form-group">
              <input type="email" class="form-control email" placeholder="Enter email">
              <input type="submit" class="btn btn-primary submit" value="Subscribe">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <footer class="site-footer">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
          <h3>Clear Smile</h3>
          <p>Invisible Aligners</p>
        </div>
        <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
          <h3 class="heading">Quick Link</h3>
          <div class="row">
            <div class="col-md-6">
              <ul class="list-unstyled">
                <li><a href="#">Home</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#">Courses</a></li>
                <li><a href="#">Pages</a></li>
              </ul>
            </div>
            <div class="col-md-6">
              <ul class="list-unstyled">
                <li><a href="#">News</a></li>
                <li><a href="#">Support</a></li>
                <li><a href="#">Contact</a></li>
                <li><a href="#">Privacy</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
          <h3 class="heading">Blog</h3>
          <div class="block-21 d-flex mb-4">
            <div class="text">
              <h3 class="heading mb-0"><a href="#">Consectetur Adipisicing Elit</a></h3>
              <div class="meta">
                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
              </div>
            </div>
          </div>  
          <div class="block-21 d-flex mb-4">
            <div class="text">
              <h3 class="heading mb-0"><a href="#">Dolore Tempora Consequatur</a></h3>
              <div class="meta">
                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
              </div>
            </div>
          </div>  
          <div class="block-21 d-flex mb-4">
            <div class="text">
              <h3 class="heading mb-0"><a href="#">Perferendis eum illum</a></h3>
              <div class="meta">
                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
              </div>
            </div>
          </div>  
        </div>
        <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
          <h3 class="heading">Contact Information</h3>
          <div class="block-23">
            <ul>
              <li><span class="icon ion-android-pin"></span><span class="text">Unit No. 713, Block A, Kelana Centre Point,
                No. 3, Jalan SS7/19, Kelana Jaya,
                47301 Petaling Jaya, Selangor, Malaysia</span></li>
              <li><a href="#"><span class="icon ion-ios-telephone"></span><span class="text">+603-7625 9774</span></a></li>
              <li><a href="#"><span class="icon ion-android-mail"></span><span class="text">care.my@clearsmile.asia</span></a></li>
              <li><span class="icon ion-android-time"></span><span class="text">Monday &mdash; Friday 8:00am - 5:00pm</span></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row pt-5">
        <div class="col-md-12 text-center copyright">
          
          <p class="float-md-left"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" class="text-primary">Colorlib</a><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
          <p class="float-md-right">
            <a href="#" class="fa fa-facebook p-2"></a>
            <a href="#" class="fa fa-twitter p-2"></a>
            <a href="#" class="fa fa-linkedin p-2"></a>
            <a href="#" class="fa fa-instagram p-2"></a>

          </p>
        </div>
      </div>
    </div>
  </footer>
<!-- END footer -->
  
  <!-- loader -->
  <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

  <script src="../js/jquery-migrate-3.0.0.js"></script>
  <script src="../js/popper.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
  <script src="../js/owl.carousel.min.js"></script>
  <script src="../js/jquery.waypoints.min.js"></script>
  <script src="../js/jquery.stellar.min.js"></script>
  <script src="../js/jquery.animateNumber.min.js"></script>
  <script src="../js/jquery.magnific-popup.min.js"></script>
  <script src="../js/main.js"></script>
</body>
</html>