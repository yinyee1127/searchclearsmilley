<?php

$xmlDoc=new DOMDocument();
$xmlDoc->load("../dataset.xml");
$name = isset($_POST['value']) ? $_POST['value'] : null;
$query = isset($_POST['query']) ? $_POST['query'] : null;
$record=$xmlDoc->getElementsByTagName('record');
$response=[];

if(empty($name)) {
  echo json_encode([]);
}

if($query==="doctor_name") {
  $array = [];
  //lookup all links from the xml file if length of q>0
  if (strlen($name) > 0) {
    $search = "";
    for ($i = 0; $i < 1000; $i++) {
      $clinic_name = $record->item($i)->getElementsByTagName('clinic_name');
      if ($clinic_name->item(0)->nodeType == 1) {
        if (stristr($clinic_name->item(0)->nodeValue, $name)) {
          $search =  $clinic_name->item(0)->nodeValue . "";
          // Insert data into Array
          if (!in_array($search, $array)) {
            $array[] = $search;
          } else {
          }
        }
      }
    }
  }
  foreach ($array as $value) {
    $response[] = array("label" => $value);
  }
  echo json_encode($response);
}
else if($query==="clinic_name") {
  $array = [];
  //lookup all links from the xml file if length of q>0
  if(strlen($name)>0){
    $search = "";
    for($i=0; $i<1000; $i++){
      $clinic_name=$record->item($i)->getElementsByTagName('clinic_name');
      if($clinic_name->item(0)->nodeType==1){
        if(stristr($clinic_name->item(0)->nodeValue,$name)) {
          $search =  $clinic_name->item(0)->nodeValue ."";        
          // Insert data into Array
          if(!in_array($search,$array)){
            $array[] = $search;
          } else { }
        }
      }
    }
  }
  foreach ($array as $value){
    $response[] = array("label"=>$value);
  }
  echo json_encode($response);
}
else if($query==="location") {
  $array = [];
  //lookup all links from the xml file if length of q>0
  if(strlen($name)>0){
    $search = "";
    for($i=0; $i<1000; $i++){
      $city=$record->item($i)->getElementsByTagName('city');
      if($city->item(0)->nodeType==1){
        if(stristr($city->item(0)->nodeValue,$name)){

          $search = 
          $city->item(0)->nodeValue ."";        
          // Insert data into Array
          
          if(!in_array($search,$array)){
            $array[] = $search;
          } else {

          }
        }
      }
    }
  }
  foreach ($array as $value){
    $response[] = array("label"=>$value);
  }
  echo json_encode($response);
}
else if($query==="treatment_option") {
  echo json_encode([
    "Dental bridge",
    "Dental crown",
    "Dental filling",
    "Dental implant",
    "Root canal treatment",
    "Scale and polish",
    "Tooth brace",
    "Wisdom tooth removal",
    "Laser Teeth Whitening",
    "Opalescence Teeth Whitening",
    "Teeth Bleaching",
    "Bonding & composite resins",
    "Dental denture",
    "Dental veneer",
  ]);
}

