<?php
$xmlDoc=new DOMDocument();
$xmlDoc->load("../dataset.xml");

$record=$xmlDoc->getElementsByTagName('record');

$search = "";
for($i=0; $i<8; $i++){
  $clinic_name=$record->item($i)->getElementsByTagName('clinic_name');
  $city=$record->item($i)->getElementsByTagName('city');
  $website=$record->item($i)->getElementsByTagName('website');
  if($clinic_name->item(0)->nodeType==1){
      $search = 
      '
    <div class="col-sm-6 col-md-4 col-lg-3">
      <div class="media block-6 d-block">
        <figure>
          <a href="#feat.html"><img src="images/clinic2.jpg" alt="Image" class="img-fluid"></a>
        </figure>
        <div class="media-body" id="feat1">
          <h3 class="heading" style="margin:auto">'. $clinic_name->item(0)->nodeValue .'</h3>
          <p style="margin:auto"><i class="fa fa-map-marker padding10">'. $city->item(0)->nodeValue .'</i></p>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star checked"></span>
          <span class="fa fa-star"></span>
          <span class="fa fa-star"></span>
          <p><a href="#feat.html" class="more">More Details<span class="ion-arrow-right-c"></span></a></p>
        </div>
      </div>
    </div>
    ';

      // $clinic_name->item(0)->nodeValue ."\n/". 
      // "City: ".$city->item(0)->nodeValue; 

      // Insert data into Array
      $array[] = $search;
  }
}

// Sample for feature dentist
    // <div class="col-md-6 col-lg-3">
    //     <div class="media block-6 d-block">
    //         <figure>
    //             <a href="course-single.html"><img src="images/clinic2.jpg" alt="Image" class="img-fluid"></a>
    //         </figure>
    //         <div class="media-body" id="feat1">
    //             <h3 class="heading" style="margin:auto" id="doc_name">Boren Medical Hub</h3>
    //             <p style="margin:auto"><i class="fa fa-map-marker padding10">Bangsar, Kuala Lumpur</i></p>
    //             <span class="fa fa-star checked"></span>
    //             <span class="fa fa-star checked"></span>
    //             <span class="fa fa-star checked"></span>
    //             <span class="fa fa-star"></span>
    //             <span class="fa fa-star"></span>
    //             <p><a href="#" class="more">More Details<span class="ion-arrow-right-c"></span></a></p>
    //         </div>
    //     </div> 
    // </div>
// End Here

foreach($array as $value){
echo $value;
}
?>