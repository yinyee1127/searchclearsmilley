<!doctype html>
<html lang="en">

<head>
    <title>Free Education Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/site.webmanifest">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">

    <link rel="stylesheet" href="fonts/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <style>
        .checked {
            color: orange;
        }
    </style>
    <!-- Theme Style -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

    <header role="banner">

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">

                <img style=" max-width:150px;
    height:auto;" src="images/black.png" alt="Image placeholder" class="img-fluid">



                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
                    <ul class="navbar-nav mx-auto" style="margin-right:200px !important">
                        <a class="nav-link active" href="home.html">Home</a>
                        </li>




                        <li class="nav-item">
                            <a class="nav-link" href="about.html">About Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contact.html">Contact Us</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav absolute-right">
                        <li>
                            <a href="login.html">Login</a> / <a href="register.html">Register</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
    </header>
    <!-- END header -->

    <div class="block-17" style="margin:20px">
        <form action="" method="post" class="d-block d-lg-flex">
            <div class="fields d-block d-lg-flex">
                <div class="textfield-search one-third "><input type="text" class="form-control" placeholder="Search by location..."></div>
                <div class="textfield-search one-third "><input type="text" class="form-control" placeholder="Search by clinic name..."></div>
                <div class="textfield-search one-third "><input type="text" class="form-control" placeholder="Search by doctor name..."></div>
                <div class="textfield-search one-third "><input type="text" class="form-control" placeholder="Search by treatment..."></div>
                <!-- width percentage100 -->
            </div>
            <input type="submit" class="search-submit btn btn-primary" value="Search">
        </form>
    </div>
    <!-- END section -->

    <section class="site-section pt-3 element-animate" style="padding-bottom:0px">

        <div class="container">
            <div class="row element-animate">
                <div class="col-md-7 text-center section-heading">
                    <h2 class="text-primary heading" style="font-size:28px;text-align:left">Kilnik Pengigian Katte</h2>
                    <!-- <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Suscipit qui neque sint eveniet tempore sapiente.</p> -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <!-- featured clinic -->
                    <div class="row">
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="media block-6 d-block">
                                <figure>
                                    <a href="course-single.html"><img src="images/klinik2.jpg" alt="Image" class="img-fluid"></a>
                                </figure>

                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="media block-6 d-block">
                                <figure>
                                    <a href="course-single.html"><img src="images/klinik3.jpg" alt="Image" class="img-fluid"></a>
                                </figure>

                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="media block-6 d-block">
                                <figure>
                                    <a href="course-single.html"><img src="images/klinik4.jpg" alt="Image" class="img-fluid"></a>
                                </figure>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="site-section pt-3 element-animate" style="padding-bottom:0px">

        <div class="container">
            <div class="row element-animate">
                <div class="col-md-9 section-heading">
                    <h2 class="text-primary heading" style="font-size:28px">Klinik Pergigian Katte</h2>
                    <p>Pandan Jaya</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <!-- featured clinic -->
                    <div class="row">
                        <div class="col-sm-6 col-md-9 col-lg-9">
                            <div class="media block-6 d-block" style="text-align:left">
                                <i class="fa fa-map-marker padding10">40A, Jalan Pandan 2/1, Pandan Jaya, 55100 Kuala Lumpur, Selangor, Malaysia.</i><br>
                                <i class="fa fa-phone padding10">Contact No: 604-245-2768</i><br>
                                <i class="fa fa-envelope padding10">Email Address : info@dentist-lunn.com</i><br><br>
                                <h5>Attending Doctors:</h5>
                                <p>Dr.Kathiravan Purmal
                                    (Orthodontist Oral & Maxillofacial Surgeon)<br>

                                    Dr. Chanderan Shanmugam
                                    (Visiting Dentist)<br>

                                    Dr.Bindu Sharma
                                    (Primary Care Dentist) <br>
                                </p>
                                <h5>Consultation Hours</h5>
                                <p>Monday-Friday - 9.00 AM - 5.00 PM<br>
                                    Saturday - 9.00 AM - 6.00 PM<br>
                                    Sunday, Public Holiday - Closed
                                </p>
                                <h6>This clinic accept patient by:</h6>
                                <p>Walk-in or by appointment.</p>
                                <p>Treatment Provided: Orthodontics, Minor Oral Surgery, Implant Treatment, Extractions, Major Dental Surgery in hospital, Crown and Bridge, Denture, Scaling and Polishing</p>
                                <p>Please call in advance to make sure the treatment is available to avoid dissapointment.</p>


                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3 col-lg-3">
                            <div class="media block-6 d-block">
                                <figure>
                                    <img src="https://via.placeholder.com/250x643" alt="Ad">
                                </figure>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="site-section pt-3 element-animate">
        <div class="container">
            <div class="row element-animate">
                <div class="col-md-9 section-heading">
                    <h2 class="text-primary heading" style="font-size:28px">Reviews</h2>

                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-lg-9">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">

                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            3.56 &emsp;
                            77 reviews
                            <p>Communication:&emsp;3.6<span class="fa fa-star checked"></span></p>
                            <p>Check-in:&emsp;4.9<span class="fa fa-star checked"></span></p>
                            <p>Value:&emsp;5.0<span class="fa fa-star checked"></span></p>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6" style="padding-left:0px">
                            <div class="fields d-block d-lg-flex">
                                <div class="textfield-search "><input type="text" class="form-control" placeholder="Search Reviews..."></div>
                                <input type="submit" class="search-submit btn btn-primary" value="Search">
                            </div>


                            <p>Accuracy:&emsp;3.6<span class="fa fa-star checked"></span></p>
                            <p>Location:&emsp;4.9<span class="fa fa-star checked"></span></p>
                            <p>Cleanliness:&emsp;5.0<span class="fa fa-star checked"></span></p>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">

                            <p>User:</p>
                            <p>Its a good place to get my teeth done. I would rate this 5 stars!</p><br>
                            <div class="row element-animate">
                                <div class="col-md-9 section-heading">
                                    <h2 class="text-primary heading" style="font-size:28px">Location</h2>
                                    <p>Area Details.</p>
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </section>
    <!-- END section -->



    <div class="py-5 block-22">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 mb-4 mb-md-0 pr-md-5">
                    <h2 class="heading">Create cool websites</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi accusantium optio und.</p>
                </div>
                <div class="col-md-6">
                    <form action="#" class="subscribe">
                        <div class="form-group">
                            <input type="email" class="form-control email" placeholder="Enter email">
                            <input type="submit" class="btn btn-primary submit" value="Subscribe">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <footer class="site-footer">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                    <h3>University</h3>
                    <p>Perferendis eum illum voluptatibus dolore tempora consequatur minus asperiores temporibus.</p>
                </div>
                <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                    <h3 class="heading">Quick Link</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Courses</a></li>
                                <li><a href="#">Pages</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li><a href="#">News</a></li>
                                <li><a href="#">Support</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                    <h3 class="heading">Blog</h3>
                    <div class="block-21 d-flex mb-4">
                        <div class="text">
                            <h3 class="heading mb-0"><a href="#">Consectetur Adipisicing Elit</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 d-flex mb-4">
                        <div class="text">
                            <h3 class="heading mb-0"><a href="#">Dolore Tempora Consequatur</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 d-flex mb-4">
                        <div class="text">
                            <h3 class="heading mb-0"><a href="#">Perferendis eum illum</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                    <h3 class="heading">Contact Information</h3>
                    <div class="block-23">
                        <ul>
                            <li><span class="icon ion-android-pin"></span><span class="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
                            <li><a href="#"><span class="icon ion-ios-telephone"></span><span class="text">+2 392 3929 210</span></a></li>
                            <li><a href="#"><span class="icon ion-android-mail"></span><span class="text">info@yourdomain.com</span></a></li>
                            <li><span class="icon ion-android-time"></span><span class="text">Monday &mdash; Friday 8:00am - 5:00pm</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-md-12 text-center copyright">

                    <p class="float-md-left">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" class="text-primary">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                    <p class="float-md-right">
                        <a href="#" class="fa fa-facebook p-2"></a>
                        <a href="#" class="fa fa-twitter p-2"></a>
                        <a href="#" class="fa fa-linkedin p-2"></a>
                        <a href="#" class="fa fa-instagram p-2"></a>

                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- END footer -->

    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214" /></svg></div>

    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/jquery-migrate-3.0.0.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.stellar.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
    <script src="js/google-map.js"></script>


    <script src="js/main.js"></script>
</body>

</html>