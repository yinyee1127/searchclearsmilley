<?php
$email = "";
$errors = [];

$conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_PATH);

// SIGN UP USER
if (isset($_POST['register'])) {
  $f_name   = (isset($_POST["f_name"]) and !empty($_POST["f_name"]))   ? $_POST["f_name"]  : $errors['f_name'] = 'Please fill in your Full Name... ';
  $ic       = (isset($_POST["ic"]) and !empty($_POST["ic"]))           ? $_POST["ic"]      : $errors['ic'] = 'Please fill in your IC... ';
  $address  = (isset($_POST["address"]) and !empty($_POST["address"])) ? $_POST["address"] : $errors['address'] = 'Please fill in your Address... ';
  $contact  = (isset($_POST["contact"]) and !empty($_POST["contact"])) ? $_POST["contact"] : $errors['contact'] = 'Please fill in your Contact No... ';
  $email    = (isset($_POST["email"]) and !empty($_POST["email"]))     ? $_POST["email"]   : $errors['email'] = 'Please fill in your Email Address... ';
  $password = (isset($_POST["password"]) and !empty($_POST["password"])) ? password_hash($_POST['password'], PASSWORD_DEFAULT) : $errors['password'] = 'Please enter your Password... ';
   // generate unique token
  $token = bin2hex(random_bytes(50));
  // Retype Password validation
  if (isset($_POST['password']) && $_POST['password'] !== $_POST['retype-password']) { $errors['retype-password'] = 'Two password does not match '; }
  
  if (!preg_match( "/^[a-zA-Z ]*$/", $f_name )) {
    $errors['f_name'] = "Only Letters and space allowed for Full Name ";
    $f_name = "";
  }
  if (!preg_match( "/^[0-9]+$/", $ic )) {
    $errors['ic'] = "Please insert only numbers for your IC number ";
    $contact = "";
  }
  if (!preg_match( "/^[0-9]+$/", $contact )) {
    $errors['contact'] = "Please insert only numbers for Contact ";
    $contact = "";
  }
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors['email'] = "Please insert your real email address ";
  }

  // Check if email already exists
  $sql = "SELECT * FROM users WHERE email='$email' LIMIT 1";
  $result = mysqli_query($conn, $sql);
  if (mysqli_num_rows($result) > 0) {
    $errors['email'] = "Email already exists ";
  }
  
  if (count($errors) === 0) {
    $query = "INSERT INTO users SET f_name=?, ic=?, address=?, contact=?, email=?, token=?, password=?";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('sisisss', $f_name, $ic, $address, $contact, $email, $token, $password);
    $result = $stmt->execute();

    if ($result) {
      $user_id = $stmt->insert_id;
      $stmt->close();

      // TO DO: send verification email to user
      // sendVerificationEmail($email, $token);

      $_SESSION['id'] = $user_id;
      $_SESSION['email'] = $email;
      $_SESSION['verified'] = false;
      $_SESSION['message'] = 'You are logged in! ';
      header('location: /');
    } else {
      $_SESSION['error_msg'] = "Database error: Could not register user ";
    }
  }
}

// LOGIN
if (isset($_POST['login'])) {
  $email    = (isset($_POST["email"]) and !empty($_POST["email"]))     ? $_POST["email"]   : $errors['email'] = 'Email is required';
  $password = (isset($_POST["password"]) and !empty($_POST["password"])) ? password_hash($_POST['password'], PASSWORD_DEFAULT) : $errors['password'] = 'Password is required';

  if (count($errors) === 0) {
    $query = "SELECT * FROM users WHERE email=? LIMIT 1";
    $stmt = $conn->prepare($query);
    $stmt->bind_param('s',$email);

    if ($stmt->execute()) {
      $result = $stmt->get_result();
      $user = $result->fetch_assoc();
      if ($user !== NULL && password_verify($password, $user['password'])) {   // if password matches
        $stmt->close();

        $_SESSION['id'] = $user['id'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['verified'] = $user['verified'];
        $_SESSION['message'] = 'You are logged in!';
        $_SESSION['type'] = 'alert-success';
        header('location: /');
        exit(0);
      } else if ( $user == NULL || !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
        $errors['login_email'] = "Incorrect Email ";
      } else { // if password does not match
        $errors['login_fail'] = "Incorrect Password ";
      }
    } else {
      $_SESSION['message'] = "Database error. Login failed!";
      $_SESSION['type'] = "alert-danger";
    }
  }
}
