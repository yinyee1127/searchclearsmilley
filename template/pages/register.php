<?php
require_once __DIR__."/controller.php";

use Hybridauth\Hybridauth;

$hybridauth = new Hybridauth($CONFIG_SOCIAL2);
$adapters = $hybridauth->getConnectedAdapters();

if (isset($adapters)) {
  foreach ($adapters as $name => $adapter) {
    $f_name = $adapter->getUserProfile()->displayName;
    $token = bin2hex(random_bytes(50));
    $email = $adapter->getUserProfile()->email;
    $ic = "";
    $address = "";
    $contact = "";
    $password = "";

    // Check if email already exists
    $sql = "SELECT * FROM users WHERE email='$email' LIMIT 1";
    $result = mysqli_query($conn, $sql);
    if (mysqli_num_rows($result) > 0) {
        $errors['email'] = "Email already exists ";
        $adapter->disconnect();
    }

    if (count($errors) === 0) {
      $query = "INSERT INTO users SET f_name=?, ic=?, address=?, contact=?, email=?, token=?, password=?";
      $stmt = $conn->prepare($query);
      $stmt->bind_param('sisisss', $f_name, $ic, $address, $contact, $email, $token, $password);
      $result = $stmt->execute();

      if ($result) {
      $user_id = $stmt->insert_id;
      $stmt->close();
      }
    }
  }
}
?>

<!doctype html>
<html lang="en">

<head>
    <!-- START page_head_start_tag -->
    <?php
    load_page_fragement(
        "page_head_start_tag",
        [
            "page_title" => "Findentist",
        ]
    );
    ?>
    <!-- END page_head_start_tag -->
</head>

<body>
<?php
if ( count($errors) > 0 ) {
    echo "<script type='text/javascript'>alert('";
    foreach ($errors as $error){
      echo $error;
    }
    echo "');</script>";
}
?>
    <style>
        /* ONLY FOR DESKTOP AND LAPTOP */
        .site-hero .navbar .header-logo {
            max-width: 150px !important;
            height: auto !important;
        }

        /* TODO: change logo to color.png on hover */
        .site-hero .header-logo:hover {
            max-width: 150px !important;
            height: auto !important;
        }

        .site-hero .navbar-nav {
            margin-right: 0;
            margin-left: auto;
        }

        .site-hero .navbar-nav a {
            font-weight: 600 !important;
            text-decoration: none !important;
            color: white !important;
        }

        .site-hero .navbar-nav .nav-item.active {
            border-bottom: 2px solid white;
        }

        .site-hero .navbar-nav .nav-item:hover {
            border-bottom: 2px solid #11CBD7 !important;
        }

        .site-hero .nav-item .dropdown-menu .dropdown-item {
            color: black !important;
            font-weight: 600 !important;
        }

        .site-hero .nav-item .dropdown-header {
            font-weight: 600;
            letter-spacing: 1px;
        }

        .site-hero .nav-item .dropdown-item.submenu:hover {
            position: absolute;
            top: 63%;
            left: 10%;
            padding: 0.5rem 0;
            margin: 0.125rem 0 0;
        }

        .site-hero .container:nth-child(1) {
            margin: 0 auto !important;
            padding: 0 !important;
        }

        .site-hero header[role=banner] {
            box-shadow: none;
        }

        .site-hero header[role=banner] nav {
            background: linear-gradient(180deg, rgba(0, 0, 0, 0.224) 0%, rgba(0, 0, 0, 0) 100%) !important;
        }

        .site-hero .form-wrap .line-height-26 {
            line-height: 26px !important;
        }

        .site-hero .form-wrap .line-height-36 {
            line-height: 36px !important;
        }

        .site-hero .form-wrap .block-bottom-8 {
            margin-bottom: 8px !important;
            padding: 0 !important;
        }

        .site-hero .form-wrap .block-bottom-16 {
            margin-bottom: 16px !important;
            padding: 0 !important;
        }

        .site-hero .form-wrap a {
            text-align: right;
            display: inline-block;
            width: 100%;
            padding: 0;
            vertical-align: super;
            font-size: smaller;
            text-decoration: underline;
        }

        .site-hero .form-wrap input {
            border-radius: 0.25rem !important;
        }

        .text-section {
            color: rgba(0, 0, 0, 0.8) !important;
            letter-spacing: 0.05rem;
            padding-left: 8px;
        }


        @media screen and (max-width: 320px) {
            .feature-thumbnail {
                height: 304px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 576px) {
            .ads-standard-banner {}

            .ads-half-banner {}

            .ads-leaderboard {}

            /* This is how it looks like when screen more that 1440px */
            .site-hero .container:nth-child(1) {
                width: 100vw !important;
                max-width: 1440px !important;
            }

            .feature-thumbnail {
                height: 254px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 768px) {
            .feature-thumbnail {
                height: 183.99px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 992px) {
            .feature-thumbnail {
                height: 183.99px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 1200px) {
            .feature-thumbnail {
                height: 221.49px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }


        .container-header {
            background: linear-gradient(180deg, rgba(0, 0, 0, 0.224) 0%, rgba(0, 0, 0, 0) 100%) !important;
        }

        .container-header header {
            box-shadow: none;
        }

        .container-header header .navbar {
            height: 64px;
            background: none !important
        }

        .container .form-wrap {
            border-radius: 4px !important;
            padding: 32px !important;
            width: 508px !important;
            margin-top: 0 !important;
        }

        .container .form-wrap h2 {
            font-size: 32px;
        }

        .container .form-wrap .description {
            font-size: 18px;
        }

        .container input[type=submit] {
            width: 100%;
        }

        h2.heading {
            margin: 0px !important;
            word-wrap: break-word !important;
            font-family: Circular, -apple-system, BlinkMacSystemFont, Roboto, Helvetica Neue, sans-serif !important;
            font-size: 24px !important;
            font-weight: 800 !important;
            line-height: 1.25em !important;
            color: #484848 !important;
            padding-top: 2px !important;
            padding-bottom: 2px !important;
            margin-top: 0 !important;
            margin-bottom: 16px !important;
        }

        h2.heading {
            margin: 0px !important;
            word-wrap: break-word !important;
            font-family: Circular, -apple-system, BlinkMacSystemFont, Roboto, Helvetica Neue, sans-serif !important;
            font-size: 24px !important;
            font-weight: 800 !important;
            line-height: 1.25em !important;
            color: #484848 !important;
            padding-top: 2px !important;
            padding-bottom: 2px !important;
            margin-top: 0 !important;
            margin-bottom: 16px !important;
        }

        /* ONLY FOR DESKTOP AND LAPTOP */
    </style>
    <!-- START page_top_navigation -->
    <?php load_page_fragement("page_header_simple", [
        "nav_active_home_page" => is_page("home") ? "active" : "",
        "nav_active_about_us_page" => is_page("about_us") ? "active" : "",
        "nav_active_services_page" => is_page("services") ? "active" : "",
        "nav_active_treatment_page" => is_page("treatments") ? "active" : "",
        "nav_active_contact_us_page" => is_page("contact_us") ? "active" : "",
        "nav_active_search_result_page" => is_page("search_result") ? "active" : "",
        "url_home_page" => "/",
        "url_about_us_page" => "/about-us",
        "url_services_page" => "/services",
        "url_treatment_page" => "/treatments",
        "url_contact_us_page" => "/contact-us",
        "url_search_result_page" => "/search-result",
        "url_clinic_page" => "/clinic-page",
        "url_login_page" => "/login",
        "url_register_page" => "/register",
    ]); ?>
    <!-- END page_top_navigation -->
<?php
// foreach ($errors as $error) {
// echo $error."<br>";
// }
?>
    <!-- Register Section -->
    <?php load_page_fragement("section_register"); ?>
    <!-- Register Section -->

    <!-- START page_footer -->
    <?php load_page_fragement("page_footer"); ?>
    <!-- END page_footer -->

    <!-- START </body> script -->
    <?php load_page_fragement("page_body_end_tag", [
        "url_search_autocomplete" => "/search-autocomplete",
    ]); ?>
    <!-- END </body> script -->
    <?php $adapter->disconnect(); ?>
</body>

</html>