<!doctype html>
<html lang="en">

<head>
    <!-- START page_head_start_tag -->
    <?php
    load_page_fragement(
        "page_head_start_tag",
        [
            "page_title" => "Findentist",
        ]
    );
    ?>
    <!-- END page_head_start_tag -->
</head>

<body>
    <style>
        /* ONLY FOR DESKTOP AND LAPTOP */
        .site-hero .navbar .header-logo {
            max-width: 150px !important;
            height: auto !important;
        }

        /* TODO: change logo to color.png on hover */
        .site-hero .header-logo:hover {
            max-width: 150px !important;
            height: auto !important;
        }

        .site-hero .navbar-nav {
            margin-right: 0;
            margin-left: auto;
        }

        .site-hero .navbar-nav a {
            font-weight: 600 !important;
            text-decoration: none !important;
            color: white !important;
        }

        .site-hero .navbar-nav .nav-item.active {
            border-bottom: 2px solid white;
        }

        .site-hero .navbar-nav .nav-item:hover {
            border-bottom: 2px solid #11CBD7 !important;
        }

        .site-hero .nav-item .dropdown-menu .dropdown-item {
            color: black !important;
            font-weight: 600 !important;
        }

        .site-hero .nav-item .dropdown-header {
            font-weight: 600;
            letter-spacing: 1px;
        }

        .site-hero .nav-item .dropdown-item.submenu:hover {
            position: absolute;
            top: 63%;
            left: 10%;
            padding: 0.5rem 0;
            margin: 0.125rem 0 0;
        }

        .site-hero .container:nth-child(1) {
            margin: 0 auto !important;
            padding: 0 !important;
        }

        .site-hero header[role=banner] {
            box-shadow: none;
        }

        .site-hero header[role=banner] nav {
            background: linear-gradient(180deg, rgba(0, 0, 0, 0.224) 0%, rgba(0, 0, 0, 0) 100%) !important;
        }

        .site-hero .form-wrap .line-height-26 {
            line-height: 26px !important;
        }

        .site-hero .form-wrap .line-height-36 {
            line-height: 36px !important;
        }

        .site-hero .form-wrap .block-bottom-8 {
            margin-bottom: 8px !important;
            padding: 0 !important;
        }

        .site-hero .form-wrap .block-bottom-16 {
            margin-bottom: 16px !important;
            padding: 0 !important;
        }

        .site-hero .form-wrap a {
            text-align: right;
            display: inline-block;
            width: 100%;
            padding: 0;
            vertical-align: super;
            font-size: smaller;
            text-decoration: underline;
        }

        .site-hero .form-wrap input {
            border-radius: 0.25rem !important;
        }

        .text-section {
            color: rgba(0, 0, 0, 0.8) !important;
            letter-spacing: 0.05rem;
            padding-left: 8px;
        }


        @media screen and (max-width: 320px) {
            .feature-thumbnail {
                height: 304px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 576px) {
            .ads-standard-banner {}

            .ads-half-banner {}

            .ads-leaderboard {}

            /* This is how it looks like when screen more that 1440px */
            /* .site-hero .container:nth-child(1) {
                width: 100vw !important;
                max-width: 1440px !important;
            }
            */
            .feature-thumbnail {
                height: 254px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 768px) {
            .feature-thumbnail {
                height: 183.99px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 992px) {
            .feature-thumbnail {
                height: 183.99px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 1200px) {
            .feature-thumbnail {
                height: 221.49px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }


        .container-header {
            background: linear-gradient(180deg, rgba(0, 0, 0, 0.224) 0%, rgba(0, 0, 0, 0) 100%) !important;
        }

        .container-header header {
            box-shadow: none;
        }

        .container-header header .navbar {
            height: 64px;
            background: none !important
        }

        .container .form-wrap {
            border-radius: 4px !important;
            padding: 32px !important;
            width: 508px !important;
            margin-top: 0 !important;
        }

        .container .form-wrap h2 {
            font-size: 32px;
        }

        .container .form-wrap .description {
            font-size: 18px;
        }
        /* ONLY FOR DESKTOP AND LAPTOP */
    </style>
    
    <!-- START page_top_navigation -->
   <?php load_page_fragement("page_header_simple", [
    "url_post_search_result" => "/search-result",
    "nav_active_home_page" => is_page("home") ? "active" : "",
    "nav_active_about_us_page" => is_page("about_us") ? "active" : "",
    "nav_active_services_page" => is_page("services") ? "active" : "",
    "nav_active_treatment_page" => is_page("treatments") ? "active" : "",
    "nav_active_contact_us_page" => is_page("contact_us") ? "active" : "",
    "nav_active_search_result_page" => is_page("search_result") ? "active" : "",
    "url_home_page" => "/",
    "url_about_us_page" => "/about-us",
    "url_services_page" => "/services",
    "url_treatment_page" => "/treatments",
    "url_contact_us_page" => "/contact-us",
    "url_search_result_page" => "/search-result",
    "url_clinic_page" => "/clinic-page",
    "url_login_page" => "/login",
    "url_register_page" => "/register",
  ]); ?>
    <!-- END page_top_navigation -->

<section class="site-hero site-sm-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(images/background/b4.jpg);">
  <div class="container">
    <div class="row align-items-center justify-content-center site-hero-sm-inner">
      <div class="col-md-7 text-center">
        <div class="mb-5 element-animate">
          <h1 class="mb-2 h1 services">Teledental Services</h1>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- END section -->

<section class="site-section element-animate">
  <div class="container">   
    <div class="col-md-12">
      <div class = row>
        <div class="col-md-6">
          <div class="heading">
            <h2 class="h2 register paddingbottom16">What is Teledental Services?</h2>
          </div>
          <div class="text mb-5">
            <p  class=" p about">Lorem ipsum dolor sit amet consectetur adipisicing elit. A quibusdam nisi eos accusantium eligendi velit deleniti nihil ad deserunt rerum incidunt nulla nemo eius molestiae architecto beatae asperiores doloribus animi. Lorem ipsum dolor sit amet consectetur adipisicing elit. A quibusdam nisi eos accusantium eligendi velit deleniti nihil ad deserunt rerum incidunt nulla nemo eius molestiae architecto beatae asperiores doloribus animi.</p>
            <p class=" p about">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos quaerat aliquid blanditiis eum asperiores obcaecati, id officiis voluptate sint est excepturi quam itaque dicta delectus. Consequuntur, blanditiis maxime? Cupiditate, cum.</p>
            <p class=" p about">Porro cupiditate doloremque nihil architecto, id nisi tenetur obcaecati, harum nulla aut ipsam sunt ullam eos, ipsa odit voluptatibus veniam. Amet itaque incidunt cumque tenetur, omnis repellat non dolorem nostrum.</p>               
          </div>
        </div>
        <div class="col-md-6">
          <div class="heading">
            <h2 class="h2 register paddingbottom16">Why get a second opinion?</h2>
          </div>
          <div class="text mb-5">
            <p  class=" p about">Lorem ipsum dolor sit amet consectetur adipisicing elit. A quibusdam nisi eos accusantium eligendi velit deleniti nihil ad deserunt rerum incidunt nulla nemo eius molestiae architecto beatae asperiores doloribus animi. Lorem ipsum dolor sit amet consectetur adipisicing elit. A quibusdam nisi eos accusantium eligendi velit deleniti nihil ad deserunt rerum incidunt nulla nemo eius molestiae architecto beatae asperiores doloribus animi.</p>
            <p class=" p about">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos quaerat aliquid blanditiis eum asperiores obcaecati, id officiis voluptate sint est excepturi quam itaque dicta delectus. Consequuntur, blanditiis maxime? Cupiditate, cum. Lorem ipsum dolor sit amet consectetur adipisicing elit. A quibusdam nisi eos accusantium eligendi velit deleniti nihil ad deserunt rerum incidunt nulla nemo eius molestiae architecto beatae asperiores doloribus animi.</p>  
          </div>
        </div>
      </div>
      <div class = row>
        <div class="col-md-6">
          <div class="heading">
          <h2 class="h2 register paddingbottom16">Why Choose Our Services?</h2>
          </div>      
        </div>  
      </div>
      <div class="row">
        <div class="col-md-6 order-md-2">
          <div class="block-16">
            <figure>
              <img src="images/about1.jpg" alt="Image placeholder" class="img-fluid">
            </figure>
          </div>
        </div>
        <div class="col-md-6 order-md-1">      
          <div class="text mb-5">
            <p  class="p about">Lorem ipsum dolor sit amet consectetur adipisicing elit. A quibusdam nisi eos accusantium eligendi velit deleniti nihil ad deserunt rerum incidunt nulla nemo eius molestiae architecto beatae asperiores doloribus animi.</p>
            <p  class="p about">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos quaerat aliquid blanditiis eum asperiores obcaecati, id officiis voluptate sint est excepturi quam itaque dicta delectus. Consequuntur, blanditiis maxime? Cupiditate, cum.</p>
            <p  class="p about">Porro cupiditate doloremque nihil architecto, id nisi tenetur obcaecati, harum nulla aut ipsam sunt ullam eos, ipsa odit voluptatibus veniam. Amet itaque incidunt cumque tenetur, omnis repellat non dolorem nostrum.</p>
            <p class="p about">Click <a href="#">here</a> to find out our Teledental Service.</p>            
          </div>           
        </div>   
      </div>  
      <div class = row>
        <div class="col-md-6 col-lg-6 order-md-1">
          <div class="block-26">
            <div class="heading">
              <h2 class="h2 register">Areas of Specialty</h2>
            </div>
            <ul>
              <li class="p about"><a href="#">Root Canals</a></li>
              <li class="p about"><a href="#">Braces & Aligners</a></li>
              <li class="p about"><a href="#">Extractions/ Implants</a></li>
              <li class="p about"><a href="#">Infant / Children</a></li>
              <li class="p about"><a href="#">Gum Disease</a></li>
              <li class="p about"><a href="#">Partials / Dentures</a></li>
              <li class="p about"><a href="#">TMJ/ Facial Pain</a></li>
              <li class="p about"><a href="#">Cosmetic Dentistry</a></li>
            </ul>
          </div>
          <p class="p about">Disclaimer:  if the patient does not present with the required records, the specialist can determine that there is insufficient documentation to recommend or agree with the diagnosed treatment.</p>
        </div>
      </div>   
    </div>           
  </div>  
</section>

<!-- END section -->

    <!-- START section_subscribe_to_us -->
    <?php load_page_fragement("section_subscribe_to_us"); ?>
    <!-- END section_subscribe_to_us -->

    <!-- START page_footer -->
    <?php load_page_fragement("page_footer"); ?>
    <!-- END page_footer -->

    <!-- START </body> script -->
    <?php load_page_fragement("page_body_end_tag"); ?>
    <!-- END </body> script -->
</body>

</html>