<!doctype html>
<html lang="en">

<head>
    <!-- START page_head_start_tag -->
    <?php
    load_page_fragement(
        "page_head_start_tag",
        [
            "page_title" => "Findentist",
        ]
    );
    ?>
    <!-- END page_head_start_tag -->
</head>

<body>
    <?php
    try {
        session_start();
        if ($_SESSION['mail']) {
            echo "<script type='text/javascript'>alert('" .$_SESSION['mail']. "');</script>";
        }
        session_destroy();
    } catch (Exception $e) {
        
    }
?>
    <style>
        /* ONLY FOR DESKTOP AND LAPTOP */
        .site-hero .navbar .header-logo {
            max-width: 150px !important;
            height: auto !important;
        }

        /* TODO: change logo to color.png on hover */
        .site-hero .header-logo:hover {
            max-width: 150px !important;
            height: auto !important;
        }

        .site-hero .navbar-nav {
            margin-right: 0;
            margin-left: auto;
        }

        .site-hero .navbar-nav a {
            font-weight: 600 !important;
            text-decoration: none !important;
            color: white !important;
        }

        .site-hero .navbar-nav .nav-item.active {
            border-bottom: 2px solid white;
        }

        .site-hero .navbar-nav .nav-item:hover {
            border-bottom: 2px solid #11CBD7 !important;
        }

        .site-hero .nav-item .dropdown-menu .dropdown-item {
            color: black !important;
            font-weight: 600 !important;
        }

        .site-hero .nav-item .dropdown-header {
            font-weight: 600;
            letter-spacing: 1px;
        }

        .site-hero .nav-item .dropdown-item.submenu:hover {
            position: absolute;
            top: 63%;
            left: 10%;
            padding: 0.5rem 0;
            margin: 0.125rem 0 0;
        }

        .site-hero .container:nth-child(1) {
            margin: 0 auto !important;
            padding: 0 !important;
        }

        .site-hero header[role=banner] {
            box-shadow: none;
        }

        .site-hero header[role=banner] nav {
            background: linear-gradient(180deg, rgba(0, 0, 0, 0.224) 0%, rgba(0, 0, 0, 0) 100%) !important;
        }

        .site-hero .form-wrap .line-height-26 {
            line-height: 26px !important;
        }

        .site-hero .form-wrap .line-height-36 {
            line-height: 36px !important;
        }

        .site-hero .form-wrap .block-bottom-8 {
            margin-bottom: 8px !important;
            padding: 0 !important;
        }

        .site-hero .form-wrap .block-bottom-16 {
            margin-bottom: 16px !important;
            padding: 0 !important;
        }

        .site-hero .form-wrap a {
            text-align: right;
            display: inline-block;
            width: 100%;
            padding: 0;
            vertical-align: super;
            font-size: smaller;
            text-decoration: underline;
        }

        .site-hero .form-wrap input {
            border-radius: 0.25rem !important;
        }

        .text-section {
            color: rgba(0, 0, 0, 0.8) !important;
            letter-spacing: 0.05rem;
            padding-left: 8px;
        }


        @media screen and (max-width: 320px) {
            .feature-thumbnail {
                height: 304px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 576px) {
            .ads-standard-banner {}

            .ads-half-banner {}

            .ads-leaderboard {}

            /* This is how it looks like when screen more that  px */
            .feature-thumbnail {
                height: 254px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 768px) {
            .feature-thumbnail {
                height: 183.99px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 992px) {
            .feature-thumbnail {
                height: 183.99px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }

        @media screen and (min-width: 1200px) {
            .feature-thumbnail {
                height: 221.49px !important;
                border-radius: 4px !important;
                width: 100% !important;
            }
        }


        .container-header {
            background: linear-gradient(180deg, rgba(0, 0, 0, 0.224) 0%, rgba(0, 0, 0, 0) 100%) !important;
        }

        .container-header header {
            box-shadow: none;
        }

        .container-header header .navbar {
            height: 64px;
            background: none !important
        }

        .container .form-wrap {
            border-radius: 4px !important;
            padding: 32px !important;
            width: 508px !important;
            margin-top: 0 !important;
        }

        .container .form-wrap h2 {
            font-size: 32px;
        }

        .container .form-wrap .description {
            font-size: 18px;
        }

        h2.heading {
            margin: 0px !important;
            word-wrap: break-word !important;
            font-family: Circular, -apple-system, BlinkMacSystemFont, Roboto, Helvetica Neue, sans-serif !important;
            font-size: 24px !important;
            font-weight: 800 !important;
            line-height: 1.25em !important;
            color: #484848 !important;
            padding-top: 2px !important;
            padding-bottom: 2px !important;
            margin-top: 0 !important;
            margin-bottom: 16px !important;
        }

        h2.heading {
            margin: 0px !important;
            word-wrap: break-word !important;
            font-family: Circular, -apple-system, BlinkMacSystemFont, Roboto, Helvetica Neue, sans-serif !important;
            font-size: 24px !important;
            font-weight: 800 !important;
            line-height: 1.25em !important;
            color: #484848 !important;
            padding-top: 2px !important;
            padding-bottom: 2px !important;
            margin-top: 0 !important;
            margin-bottom: 16px !important;
        }

        /* ONLY FOR DESKTOP AND LAPTOP */
    </style>
    <!-- START page_top_navigation -->
    <?php load_page_fragement("page_header_simple", [
        "nav_active_home_page" => is_page("home") ? "active" : "",
        "nav_active_about_us_page" => is_page("about_us") ? "active" : "",
        "nav_active_services_page" => is_page("services") ? "active" : "",
        "nav_active_treatment_page" => is_page("treatments") ? "active" : "",
        "nav_active_contact_us_page" => is_page("contact_us") ? "active" : "",
        "nav_active_search_result_page" => is_page("search_result") ? "active" : "",
        "url_home_page" => "/",
        "url_about_us_page" => "/about-us",
        "url_services_page" => "/services",
        "url_treatment_page" => "/treatments",
        "url_contact_us_page" => "/contact-us",
        "url_search_result_page" => "/search-result",
        "url_clinic_page" => "/clinic-page",
        "url_login_page" => "/login",
        "url_register_page" => "/register",
    ]); ?>
    <!-- END page_top_navigation -->

    <section class="site-hero site-sm-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(images/background/bg2-1656x1080.jpg);">
        <div class="container">
            <div class="row align-items-center justify-content-center site-hero-sm-inner">
                <div class="col-md-7 text-center">

                    <div class="mb-5 element-animate">
                        <h1 class="mb-2 h1 contact">Contact Us</h1>
                        <p class="p register">Don’t be a stranger! Feel free to WhatsApp us or drop us an email at the following address.</p>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- END section -->
    <section class="site-section pt-3 element-animate paddingbottom0 paddingtop64">
        <div class="container">

            <div class="row padding32">
                <div class="col-md-6 col-lg-3">
                    <div class="media block-6 d-block">
                        <figure class="size50">
                            <a href="course-single.html"><img src="images/treatment.png" alt="Image" class="img-fluid"></a>
                        </figure>
                        <div class="media-body">
                            <h3 class="heading">Our Treatments</h3>

                            <p><a href="#" class="more">Learn More<span class="ion-arrow-right-c"></span></a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="media block-6 d-block">
                        <figure class="size50">
                            <a href="course-single.html"><img src="images/tooth.png" alt="Image" class="img-fluid"></a>
                        </figure>
                        <div class="media-body">
                            <h3 class="heading">Our Services</h3>

                            <p><a href="#" class="more">Learn More<span class="ion-arrow-right-c"></span></a></p>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="media block-6 d-block">
                        <figure class="size50">
                            <a href="course-single.html"><img src="images/dentist.png" alt="Image" class="img-fluid"></a>
                        </figure>
                        <div class="media-body">
                            <h3 class="heading">About Us</h3>

                            <p><a href="#" class="more">Learn More<span class="ion-arrow-right-c"></span></a></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="media block-6 d-block">
                        <figure class="size50">
                            <a href="course-single.html"><img src="images/faq.png" alt="Image" class="img-fluid"></a>
                        </figure>
                        <div class="media-body">
                            <h3 class="heading">FAQ</h3>

                            <p><a href="#" class="more">Learn More<span class="ion-arrow-right-c"></span></a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END section -->
    <section class="site-section element-animate paddingtop32">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <p class="p heading">Have some questions?</p>
                    <p class="p subhead">We will reach you as fast as we could!</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 pr-md-5">
                    <form action="/sendemail" method="post">
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" placeholder="Your Name" class="form-control register py-2" required>
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" id="name" name="phone" placeholder="01X-XXXXXXX" pattern="01[0-9]{1}-[0-9]{7++}" minlength="11" maxlength="12" class="form-control register py-2">
                            </div>
                            <div class="col-md-4 form-group">
                                <label for="email">Email</label>
                                <input type="email" id="name" name="email" placeholder="Email Address" class="form-control register py-2">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label for="message">Write Message</label>
                                <textarea name="message" name="message" class="form-control register py-2" cols="30" rows="8" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <input type="submit" value="Send Message" class="btn btn-primary width100 height45">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">

                    <div class="block-23">
                        <h3 class="heading mb-4 p heading2">Our Contact Information</h3>
                        <ul>
                            <li><span class="icon ion-android-pin"></span><span class="text">I-01-05, 5th Floor, Block I,
                                    Setia Walk, Persiaran Wawasan,
                                    Pusat Bandar Puchong, 47160 Puchong,
                                    Selangor Darul Ehsan, Malaysia
                                </span></li>
                            <li><a href="#"><span class="icon ion-ios-telephone"></span><span class="text">+603-7625 9774</span></a></li>
                            <li><a href="#"><span class="icon ion-android-mail"></span><span class="text">care.my@clearsmile.asia</span></a></li>
                            <li><span class="icon ion-android-time"></span><span class="text">Monday &mdash; Friday 8:00am - 5:00pm</span></li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>
    </section>
    <!-- END section -->



    <div class="py-5 block-22">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 mb-4 mb-md-0 pr-md-5">
                    <h2 class="heading">Subscribe to Us!</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi accusantium optio und.</p>
                </div>
                <div class="col-md-6">
                    <form action="#" class="subscribe">
                        <div class="form-group">
                            <input type="email" class="form-control email" placeholder="Enter email">
                            <input type="submit" class="btn btn-primary submit" value="Subscribe">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <footer class="site-footer">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                    <h3>Findentist</h3>
                    <p>Invisible Aligners</p>
                </div>
                <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                    <h3 class="heading">Quick Link</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li><a href="#">Home</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Courses</a></li>
                                <li><a href="#">Pages</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-unstyled">
                                <li><a href="#">News</a></li>
                                <li><a href="#">Support</a></li>
                                <li><a href="#">Contact</a></li>
                                <li><a href="#">Privacy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                    <h3 class="heading">Blog</h3>
                    <div class="block-21 d-flex mb-4">
                        <div class="text">
                            <h3 class="heading mb-0"><a href="#">Consectetur Adipisicing Elit</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 d-flex mb-4">
                        <div class="text">
                            <h3 class="heading mb-0"><a href="#">Dolore Tempora Consequatur</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="block-21 d-flex mb-4">
                        <div class="text">
                            <h3 class="heading mb-0"><a href="#">Perferendis eum illum</a></h3>
                            <div class="meta">
                                <div><a href="#"><span class="ion-android-calendar"></span> May 29, 2018</a></div>
                                <div><a href="#"><span class="ion-android-person"></span> Admin</a></div>
                                <div><a href="#"><span class="ion-chatbubble"></span> 19</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                    <h3 class="heading">Contact Information</h3>
                    <div class="block-23">
                        <ul>
                            <li><span class="icon ion-android-pin"></span><span class="text">I-01-05, 5th Floor, Block I,
                                    Setia Walk, Persiaran Wawasan,
                                    Pusat Bandar Puchong, 47160 Puchong,
                                    Selangor Darul Ehsan, Malaysia
                                </span></li>
                            <li><a href="#"><span class="icon ion-ios-telephone"></span><span class="text">+603-7625 9774</span></a></li>
                            <li><a href="#"><span class="icon ion-android-mail"></span><span class="text">care.my@clearsmile.asia</span></a></li>
                            <li><span class="icon ion-android-time"></span><span class="text">Monday &mdash; Friday 8:00am - 5:00pm</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-md-12 text-center copyright">

                    <p class="float-md-left">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());
                        </script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" class="text-primary">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                    <p class="float-md-right">
                        <a href="#" class="fa fa-facebook p-2"></a>
                        <a href="#" class="fa fa-twitter p-2"></a>
                        <a href="#" class="fa fa-linkedin p-2"></a>
                        <a href="#" class="fa fa-instagram p-2"></a>

                    </p>
                </div>
            </div>
        </div>
    </footer>

    <!-- START </body> script -->
    <?php load_page_fragement("page_body_end_tag"); ?>
    <!-- END </body> script -->

</body>

</html>
