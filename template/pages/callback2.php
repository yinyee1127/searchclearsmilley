<?php

use Hybridauth\Exception\Exception;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Hybridauth\Storage\Session;

try {
    $hybridauth = new Hybridauth($CONFIG_SOCIAL2);
    $storage = new Session();
    if (isset($_GET['provider'])) {
        $storage->set('provider', $_GET['provider']);
    }
    if ($provider = $storage->get('provider')) {
        $hybridauth->authenticate($provider);
        $storage->set('provider', null);
    }
    if (isset($_GET['logout'])) {
        $adapter = $hybridauth->getAdapter($_GET['logout']);
        $adapter->disconnect();
    }
    if (isset($_GET['code'])){
        HttpClient\Util::redirect($CONFIG_SOCIAL2['redirect']);
    }
} catch (Exception $e) {
    echo $e->getMessage();
}
