# Folder Structure 

**Html Template File**

1. Home page, index.html
2. Search result page, searchresult.html
3. Clinic page, singlepage.html


**PHP Template and Backend File**

1. Autocomplete file, search.php
2. Search file, result.php | Ex result.php?loc=a&clinic=&name=doc&treatment=
3. Featured dentist section, feat.php

---

# Getting Started

Assuming your current working directory is ./, run 
    
    php -S localhost:80 -t /public

Five url with content text/html is defined

- /
- /about-us
- /contact-us
- /search-result
- /clinic-page
- /404 (Coming soon)
- /500 (Coming soon)

One url with content application/json is defined
- 

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).