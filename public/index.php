<?php

if (!file_exists(".." . DIRECTORY_SEPARATOR . "bootstrap.php")) {
    dd("Could not bootstrap app.");
}
else {
    require_once ".." . DIRECTORY_SEPARATOR . "bootstrap.php";
}

try {
    $base_path = realpath(__DIR__."/..");
    $template_path = $base_path.DIRECTORY_SEPARATOR."template";
    $template_page_path = $template_path.DIRECTORY_SEPARATOR."pages";
    $dataset_path = $base_path.DIRECTORY_SEPARATOR."dataset.xml";
    $uri = $_SERVER["REQUEST_URI"];
    $method = $_SERVER['REQUEST_METHOD'];
    $parsed_url = parse_url($uri);

    header('Access-Control-Allow-Origin: *');
    header("Content-type: text/html; charset=utf-8");
    if (is_request("GET", "/")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."home.php";
    } else if (is_request("GET", "/clinic-page")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."clinic_page.php";
    } else if (is_url_path("/search-result")) {
        if (is_http_method("GET")) {
            require_once $template_page_path.DIRECTORY_SEPARATOR."search_result.php";
        }
        else  if (is_http_method("POST")) {
            require_once $template_page_path.DIRECTORY_SEPARATOR."search_result.php";
        }
    } else if (is_request("POST", "/search-autocomplete")) {
        require_once $template_path.DIRECTORY_SEPARATOR. "search.php";
    } else if (is_request("GET", "/treatments")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."treatments.php";
    } else if (is_request("GET", "/contact-us")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."contact_us.php";
    } else if (is_request("GET", "/about-us")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."about_us.php";
    } else if (is_request("GET", "/services")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."services.php";
    } else if (is_url_path("/login")) {
        if (is_http_method("GET")) {
            require_once $template_page_path.DIRECTORY_SEPARATOR."login.php";
        }
        else  if (is_http_method("POST")) {
            require_once $template_page_path.DIRECTORY_SEPARATOR."login.php";
        }
    } else if (is_url_path("/register")) {
        if (is_http_method("GET")) {
            require_once $template_page_path.DIRECTORY_SEPARATOR."register.php";
        }
        else  if (is_http_method("POST")) {
            require_once $template_page_path.DIRECTORY_SEPARATOR."register.php";
        }
    } else if (is_request("GET", "/logout")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."logout.php";
    } else if (is_request("GET", "/config")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."config.php";
    } else if (is_request("GET", "/callback")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."callback.php";
    } else if (is_request("GET", "/callback2")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."callback2.php";
    } else if (is_request("POST", "/sendemail")) {
        require_once $template_page_path.DIRECTORY_SEPARATOR."sendemail.php";
    } else {
        if(defined("DEBUG") AND DEBUG AND is_request("GET", "/test")) {
            require_once $template_path . DIRECTORY_SEPARATOR . "test.php";
        }
        else {
            http_response_code(404);
            require_once $template_page_path . DIRECTORY_SEPARATOR . "404.php";
        }
    }
}
catch(\Exception $ex) {
    http_response_code(500);
    require_once $template_page_path.DIRECTORY_SEPARATOR."500.php";
}

function load_page_fragement($name, $variables = array()) {
    global $template_path;
    extract($variables);
    require_once $template_path.DIRECTORY_SEPARATOR."fragments".DIRECTORY_SEPARATOR.$name.".php";
}

function is_request($_method, $path = null) {
    global $method;
    global $parsed_url;
    if (is_null($path) and $method === $_method) {
        return true;
    } else if ($method === $_method and $parsed_url['path'] === $path) {
        return true;
    } else {
        return false;
    }
}

function is_url_path($path) {
    global $parsed_url;
    if ($parsed_url['path']===$path) {
        return true;
    } else {
        return false;
    }
}

function is_http_method($_method) {
    global $method;
    if ($method === $_method) {
        return true;
    } else {
        return false;
    }
}

function is_page($name) {
    if($name==="home" AND is_request("GET", "/")) {
        return true;
    }
    else if ($name === "about_us" and is_request("GET", "/about-us")) {
        return true;
    }
    else if ($name === "services" and is_request("GET", "/services")) {
        return true;
    }
    else if ($name === "treatments" AND is_request("GET", "/treatments")) {
        return true;
    }
    else if ($name === "contact_us" AND is_request("GET", "/contact-us")) {
        return true;
    }
    else if ($name === "search_result" and is_request("GET", "/search-result")) {
        return true;
    }
    else if ($name === "clinic_page" and is_request("GET", "/clinic-page")) {
        return true;
    }
    else {
        return false;
    }
}
