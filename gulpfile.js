var gulp = require('gulp'),
    connect = require('gulp-connect-php'),
    browserSync = require('browser-sync');

gulp.task('serve', function () {
    connect.server({
        base: './public',
        port: 8080,
        keepalive: true
    }, function () {
        browserSync({
            base: './',
            proxy: 'localhost:8080',
            open: true,
            notify: true
        });
    });
    gulp.watch(['template/**/*', 'css/**/*', 'fonts/**/*', 'images/**/*', 'js/**/*', 'public/**/*']).on('change', function () {
        browserSync.reload();
    });
});
