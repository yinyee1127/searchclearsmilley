-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 13, 2020 at 03:39 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` text NOT NULL,
  `ic` int(20) NOT NULL,
  `address` text NOT NULL,
  `contact` int(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT 0,
  `token` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `ic`, `address`, `contact`, `email`, `verified`, `token`, `password`) VALUES
(2, '', 0, '', 0, 'aa', 0, 'a78cc61f1bb9b3a0c454353b94604bd1f3b747305fff7bf5940d5079dda803c2e1101808adccf11cc3c3505af5761ecb0d2e', '$2y$10$CaO4mqwSw4QfTjTbCWXRyOUlqEnAoarvUc9zzKba4m47ZtnooVXvi'),
(3, '', 0, '', 0, 'ab', 0, 'a3f1535b9a0f21e631c0e64daadda0fdee58a3cf9837ad847a6df5c80cbb50ecac938663c6153317a248aa3f701508e9ac46', '$2y$10$MTD9FNAsR/h..A2mJy5fu.g70kue6RrNoNoGagsnzOLgmlKtFKn/S'),
(4, 'aa', 111, 'aaa', 111, 'a', 0, '262a41a738238ce09c730165820d1a9a3a11642648bb61d9fb1efcc8163b3e55c2e96e04f0613ce5900dad7175abe211b509', '$2y$10$VPE9wr0xMbG5zeUP1AKlXulpbKTQjthxO1YCNTMWT6uSrr/DcUy.y');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
